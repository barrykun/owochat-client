import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import sveltePreprocess from "svelte-preprocess";
import typescript from "@rollup/plugin-typescript";
import copy from "rollup-plugin-copy";

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require("child_process").spawn("npm", ["run", "start", "--", "--dev"], {
        stdio: ["ignore", "inherit", "inherit"],
        shell: true,
      });

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}

function placeCommitHash() {
  // TODO: make this not a huge hack
  const path = require("path");
  return {
    name: "place-commit-hash",
    resolveId(source) {
      if (source === "../my-commit-hash") {
        return source;
      }
      return null;
    },
    async load(id) {
      if (id === "../my-commit-hash") {
        const stdout = await require("child_process").execSync("git rev-parse HEAD", {
          encoding: "utf-8",
        });
        return `export default "${stdout.trim()}";`;
      }
      return null;
    },
    async writeBundle(options, bundle) {
      const stdout = await require("child_process").execSync("git rev-parse HEAD", {
        encoding: "utf-8",
      });
      const commitHash = stdout.trim();
      const base = path.dirname(options.file);
      require("fs").writeFile(path.join(base, "/commit-hash"), commitHash, (err) => {
        if (err) {
          throw err;
        }
      });
    },
  };
}

export default {
  input: "src/main.ts",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: "public/build/bundle.js",
    globals: { crypto: 1, path: 1, fs: 1 }, // TODO: cursed workaround
  },
  plugins: [
    svelte({
      // enable run-time checks when not in production
      dev: !production,
      // we"ll extract any component CSS out into
      // a separate file - better for performance
      css: (css) => {
        css.write("bundle.css");
      },
      preprocess: sveltePreprocess(),
    }),

    // If you have external dependencies installed from
    // npm, you"ll most likely need these plugins. In
    // some cases you"ll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    commonjs(),
    placeCommitHash(),
    typescript({
      sourceMap: !production,
      inlineSources: !production,
    }),
    copy({
      targets: [{ src: "node_modules/olm/olm.wasm", dest: "public/build" }],
    }),

    // In dev mode, call `npm run start` once
    // the bundle has been generated
    !production && serve(),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload("public"),

    // If we"re building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),
  ],
  watch: {
    clearScreen: false,
  },
};
