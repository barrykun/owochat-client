const APP_CACHE_NAME = "owochat-v1";

const APP_URLS = [
  "/app/favicon.png",
  "/app/manifest.webmanifest",
  "/app/index.html",
  "/app/global.css",
  "/app/stranger.png",
  "/app/build/bundle.css",
  "/app/build/bundle.js",
  "/app/fontawesome/css/all.min.css",
  "/app/fontawesome/webfonts/fa-brands-400.eot",
  "/app/fontawesome/webfonts/fa-brands-400.svg",
  "/app/fontawesome/webfonts/fa-brands-400.ttf",
  "/app/fontawesome/webfonts/fa-brands-400.woff",
  "/app/fontawesome/webfonts/fa-brands-400.woff2",
  "/app/fontawesome/webfonts/fa-regular-400.eot",
  "/app/fontawesome/webfonts/fa-regular-400.svg",
  "/app/fontawesome/webfonts/fa-regular-400.ttf",
  "/app/fontawesome/webfonts/fa-regular-400.woff",
  "/app/fontawesome/webfonts/fa-regular-400.woff2",
  "/app/fontawesome/webfonts/fa-solid-900.eot",
  "/app/fontawesome/webfonts/fa-solid-900.svg",
  "/app/fontawesome/webfonts/fa-solid-900.ttf",
  "/app/fontawesome/webfonts/fa-solid-900.woff",
  "/app/fontawesome/webfonts/fa-solid-900.woff2",
  "/api/config",
];

let config;

async function initSw() {
  const cache = await caches.open(APP_CACHE_NAME);
  await cache.addAll(APP_URLS);
  self.skipWaiting();
}

async function activateSw() {
  const configRes = await caches.match("/api/config");
  if (configRes) {
    config = await configRes.json();
  }
  await clients.claim();
}

async function reloadSw() {
  await self.registration.update();
  await initSw();
  const allClients = await clients.matchAll({ includeUncontrolled: true });
  for (const client of allClients) {
    client.postMessage({
      type: "reload",
    });
  }
}

self.addEventListener("install", (event) => {
  event.waitUntil(initSw());
});

self.addEventListener("activate", (event) => {
  event.waitUntil(activateSw());
});

function processRoutes(routes) {
  return routes.map((r) => {
    let target;
    if (typeof r[1] === "string") {
      target = (_path) => r[1];
    } else if (r[1] === null) {
      target = (_path) => null;
    } else {
      target = r[1];
    }
    return {
      segments: r[0].split("/").slice(1),
      target,
    };
  });
}

const routes = processRoutes([
  ["/", "/app/index.html"],
  ["/login", "/app/index.html"],
  ["/register", "/app/index.html"],
  ["/roster", "/app/index.html"],
  ["/emoji", "/app/index.html"],
  ["/emoji/new", "/app/index.html"],
  ["/settings", "/app/index.html"],
  ["/create/room", "/app/index.html"],
  ["/search/users", "/app/index.html"],
  ["/search/rooms", "/app/index.html"],
  ["/help", "/app/index.html"],
  ["/room/*", "/app/index.html"],
  ["/room/*/emoji", "/app/index.html"],
  ["/room/*/emoji/new", "/app/index.html"],
  ["/room/*/settings", "/app/index.html"],
  ["/user/*", "/app/index.html"],
  ["/chat/*", "/app/index.html"],
  ["/app/build/commit-hash", null],
  ["/app/**", (path) => path],
]);

function routeMatch(route, pathsegments) {
  for (let i = 0; i < pathsegments.length; i++) {
    const rs = route.segments[i];
    const ps = pathsegments[i];
    if (!rs || !ps) {
      return false;
    }
    if (rs === "**") {
      return true;
    }
    if (rs !== "*" && rs !== ps) {
      return false;
    }
  }
  return true;
}

async function cacheOrError(request) {
  const res = await caches.match(request);
  if (res) {
    return res;
  } else {
    console.error("resource at " + request.url + " should have been cached");
    return fetch(request);
  }
}

self.addEventListener("fetch", (event) => {
  const url = new URL(event.request.url);
  const pathsegments = url.pathname.split("/").slice(1);
  console.log("ROUTE", url.pathname);
  for (const route of routes) {
    if (routeMatch(route, pathsegments)) {
      const action = route.target(url.pathname);
      console.log("->", action);
      if (action) {
        event.respondWith(cacheOrError(action));
      }
      return;
    }
  }
});

self.addEventListener("message", (event) => {
  const ty = event.data.type;
  switch (ty) {
    case "reload":
      event.waitUntil(reloadSw());
      break;
  }
});
