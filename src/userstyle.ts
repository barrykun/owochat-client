import { UserInfo, UserInfoBackground } from "./services/users";

export function computeMessageStyles(info: UserInfo) {
  let bgcol = "";
  const bg = info.messageBackground;
  if (bg.enabled) {
    bgcol = ` background-color: ${bg.color}`;
  }
  return `color: ${info.textColor};${bgcol}`;
}

export function computeProfileStyles(info: UserInfo) {
  let bgcol = "";
  const bg = info.messageBackground;
  if (bg.enabled) {
    bgcol = ` background-color: ${bg.color}`;
  }
  return `color: ${info.textColor};${bgcol}`;
}

export function computeBackgroundStyles(backgroundUrl: string, bg: UserInfoBackground) {
  if (bg.enabled) {
    let bgAlign;
    if (bg.align === "top-left") {
      bgAlign = "top left";
    } else if (bg.align === "top-right") {
      bgAlign = "top right";
    } else if (bg.align === "bottom-left") {
      bgAlign = "bottom left";
    } else if (bg.align === "bottom-right") {
      bgAlign = "bottom right";
    }
    const bgUrl = bg.image ? `url(${backgroundUrl})` : "none";
    return `opacity: ${
      bg.alpha / 100
    }; background-image: ${bgUrl}; background-position: ${bgAlign}; background-repeat: ${
      bg.repeat
    }; background-size: ${bg.size}`;
  } else {
    return "";
  }
}

export function computeHeaderStyles(info: UserInfo) {
  return `color: ${info.nameColor}`;
}

export function computeRosterItemStyles(info: UserInfo) {
  if (info.messageBackground.enabled) {
    return `color: ${info.nameColor}; background-color: ${info.messageBackground.color}`;
  } else {
    return "";
  }
}
