export interface ConversationInfo {
  readonly partner: string;
  opened: boolean;
  profileOpen: boolean;
  lastMsgid: string | null;
  lastReadMsgid: string | null;
  partnerLastReadMsgid: string | null;
  lastActivity: Date | null;
}
