import { Readable, Writable, writable } from "svelte/store";

import Session from "../service/session";

export class SettingsService {
  private _session: Session;

  private _suggestionMode: string;
  private _suggestionModeStore: Writable<string>;

  public constructor(session: Session) {
    this._session = session;
    this._suggestionMode =
      localStorage.getItem(this._session.myUsername + ":suggestion-mode") || "default";
    this._suggestionModeStore = writable(this._suggestionMode);
  }

  public getSuggestionModeStore(): Readable<string> {
    return this._suggestionModeStore;
  }

  public setSuggestionMode(mode: string) {
    localStorage.setItem(this._session.myUsername + ":suggestion-mode", mode);
    this._suggestionMode = mode;
    this._suggestionModeStore.set(mode);
  }
}
