import { endpoints } from "../service/endpoints";
import { Subscriber } from "../services/subscriber";

import { writable, Writable, Readable } from "svelte/store";

import { Node, parseBbCode, fullBbCodeConfig } from "../util/body";
import { Timeout } from "../util/timeout";

import { TypeOf } from "../util/decoder";
import { serverEventT } from "../service/data";
import Session from "../service/session";

import { DatabaseService } from "./database";

const MESSAGE_GROUP_TIME_SPAN = 360000;

export interface MessageQuery {
  start: string | null;
  end: string | null;
  limit: number | null;
}

export const DEFAULT_MESSAGE_QUERY = {
  start: null,
  end: null,
  limit: null,
};

// TODO: look at types
function toUiMessage(msg: RoomMessage): UiRoomMessage;
function toUiMessage(msg: DirectMessage): UiDirectMessage;
function toUiMessage(msg: Message): any {
  let body;
  let type;
  if (msg.body) {
    if (msg.body.startsWith("/me ")) {
      type = "emote";
      body = msg.body.substring(4);
    } else {
      type = "text";
      body = msg.body;
    }
  } else {
    type = "deleted";
    body = null;
  }
  return {
    ...msg,
    type: type as UiMessageType,
    bodyParsed: body !== null ? parseBbCode(body, fullBbCodeConfig) : null,
  };
}

type UiMessageType = "emote" | "text" | "deleted";

interface UiRoomMessage extends RoomMessage {
  type: UiMessageType;
  bodyParsed: Node[] | null;
  roomname: string;
}

interface UiDirectMessage extends DirectMessage {
  type: UiMessageType;
  bodyParsed: Node[] | null;
  username: string;
}

interface Message {
  msgid: string;
  author: string;
  body: string | null;
  version: number;
  sent: Date;
}

interface MessageGroup<M extends Message> {
  author: string;
  messages: M[];
}

interface RoomMessage extends Message {
  roomname: string;
}

interface DirectMessage extends Message {
  conversation: string;
  enc: string | null;
}

interface RawDirectMessage {
  msgid: string;
  src: string;
  dst: string;
  enc: string | null;
  body: string | null;
  version: number;
  sent: Date;
}

function shouldPutMessageInGroup<M extends Message>(group: MessageGroup<M>, msg: M): boolean {
  if (group.author === msg.author) {
    const firstSent = group.messages[0].sent;
    const lastSent = group.messages[group.messages.length - 1].sent;
    const tolerance = MESSAGE_GROUP_TIME_SPAN - (lastSent.valueOf() - firstSent.valueOf());
    const firstTimeDiff = Math.abs(firstSent.valueOf() - msg.sent.valueOf());
    const lastTimeDiff = Math.abs(lastSent.valueOf() - msg.sent.valueOf());
    return (
      (firstSent <= msg.sent && lastSent >= msg.sent) ||
      firstTimeDiff <= tolerance ||
      lastTimeDiff <= tolerance
    );
  } else {
    return false;
  }
}

class MessageList<M extends Message> {
  private _messages: M[];
  private _messagesMap: Map<string, M>;
  private _messageGroups: MessageGroup<M>[];
  private _messageGroupsStore: Writable<MessageGroup<M>[]>;

  private _debounceTimeout: Timeout;

  public constructor() {
    this._messages = [];
    this._messagesMap = new Map();
    this._messageGroups = [];
    this._messageGroupsStore = writable([]);
    this._debounceTimeout = new Timeout(() => {
      this._messageGroupsStore.set(this._messageGroups);
    });
  }

  public putMessage(msg: M) {
    this._messagesMap.set(msg.msgid, msg);
    let insertAtEnd = true;
    for (let i = 0; i < this._messages.length; i++) {
      const m = this._messages[i];
      if (msg.msgid === m.msgid) {
        if (msg.version > m.version) {
          this._messages.splice(i, 1, msg);
        }
        insertAtEnd = false;
        break;
      } else if (msg.msgid < m.msgid) {
        this._messages.splice(i, 0, msg);
        insertAtEnd = false;
        break;
      }
    }
    if (insertAtEnd) {
      this._messages.push(msg);
      if (
        this._messageGroups.length > 0 &&
        shouldPutMessageInGroup(this._messageGroups[this._messageGroups.length - 1], msg)
      ) {
        this._messageGroups[this._messageGroups.length - 1].messages.push(msg);
      } else {
        this._messageGroups.push({
          author: msg.author,
          messages: [msg],
        });
      }
    } else {
      let inserted = false;
      for (let ig = 0; ig < this._messageGroups.length; ig++) {
        const messageGroup = this._messageGroups[ig];
        if (shouldPutMessageInGroup(messageGroup, msg)) {
          for (let im = 0; im < messageGroup.messages.length; im++) {
            const m = messageGroup.messages[im];
            if (msg.msgid === m.msgid) {
              if (msg.version > m.version) {
                messageGroup.messages.splice(im, 1, msg);
              }
              inserted = true;
              break;
            } else if (msg.msgid < m.msgid) {
              messageGroup.messages.splice(im, 0, msg);
              inserted = true;
              break;
            }
          }
        } else if (msg.msgid < messageGroup.messages[0].msgid) {
          const newMessageGroup = {
            author: msg.author,
            messages: [msg],
          };
          this._messageGroups.splice(ig, 0, newMessageGroup);
          break;
        }
        if (inserted) {
          break;
        }
      }
    }
    this._storeUpdate();
    // TODO: possibly limit length?
  }

  public removeMessage(msgid: string) {
    this._messagesMap.delete(msgid);
    this._messages = this._messages.filter((m) => m.msgid != msgid);
    let found = false;
    for (let ig = 0; ig < this._messageGroups.length; ig++) {
      const group = this._messageGroups[ig];
      for (let im = 0; im < group.messages.length; im++) {
        if (group.messages[im].msgid === msgid) {
          group.messages.splice(im, 1);
          found = true;
          break;
        }
      }
      if (group.messages.length === 0) {
        this._messageGroups.splice(ig, 1);
      }
      if (found) {
        break;
      }
    }
    if (found) {
      this._storeUpdate();
    }
  }

  private _storeUpdate() {
    this._debounceTimeout.schedule(1);
  }

  public getMessage(msgid: string): M | undefined {
    return this._messagesMap.get(msgid);
  }

  public getMessageGroupsStore(): Readable<MessageGroup<M>[]> {
    return this._messageGroupsStore;
  }
}

export class MessagesService {
  private _session: Session;

  private _roomMessages: Map<string, MessageList<UiRoomMessage>>;
  private _directMessages: Map<string, MessageList<UiDirectMessage>>;

  private _db: DatabaseService;

  private _subscriber: Subscriber;

  public constructor(db: DatabaseService, subscriber: Subscriber, session: Session) {
    this._db = db;
    this._subscriber = subscriber;
    this._session = session;
    this._roomMessages = new Map();
    this._directMessages = new Map();
  }

  // Accessing message lists
  public getRoomMessages(roomname: string): MessageList<UiRoomMessage> {
    let ml = this._roomMessages.get(roomname);
    if (!ml) {
      ml = new MessageList();
      this._roomMessages.set(roomname, ml);
      this.preloadRoomMessages(roomname);
    }
    return ml;
  }

  public getDirectMessages(conversation: string): MessageList<UiDirectMessage> {
    let ml = this._directMessages.get(conversation);
    if (!ml) {
      ml = new MessageList();
      this._directMessages.set(conversation, ml);
      this.preloadDirectMessages(conversation);
      this.refreshDirectMessages(conversation); // TODO: only required once per connect
    }
    return ml;
  }

  // Actions for Rooms
  public async sendRoomMessage(roomname: string, body: string): Promise<{ msgid: string }> {
    return await this._session.call(endpoints.postRoomMessage, {
      roomname,
      body,
    });
  }

  public async deleteRoomMessage(roomname: string, msgid: string) {
    return await this._session.call(endpoints.deleteRoomMessage, {
      roomname,
      msgid,
    });
  }

  public async editRoomMessage(roomname: string, msgid: string, body: string) {
    return await this._session.call(endpoints.putRoomMessage, {
      roomname,
      msgid,
      body,
    });
  }

  // Actions for DMs
  public async sendDirectMessage(username: string, body: string): Promise<string> {
    const res = await this._session.call(endpoints.postDirectMessage, {
      username,
      enc: "plain", // TODO: encryption
      body,
    });
    return res.msgid;
  }

  public async editDirectMessage(username: string, msgid: string, newBody: string) {
    await this._session.call(endpoints.putDirectMessage, {
      username,
      msgid,
      enc: "plain", // TODO: encryption
      body: newBody,
    });
  }

  public async deleteDirectMessage(username: string, msgid: string) {
    await this._session.call(endpoints.deleteDirectMessage, {
      msgid,
      username,
    });
  }

  // Loading for Rooms
  public async preloadRoomMessages(roomname: string, query: MessageQuery = DEFAULT_MESSAGE_QUERY) {
    const messages = await this._db.getRoomMessages(roomname, query.start, query.end);
    for (const message of messages) {
      const ml = this.getRoomMessages(message.roomname);
      ml.putMessage(toUiMessage(message));
    }
  }

  private async fetchRoomMessages(roomname: string, query: MessageQuery = DEFAULT_MESSAGE_QUERY) {
    const messages = await this._session.call(endpoints.getRoomMessages, {
      roomname,
      ...query,
    });
    for (const message of messages) {
      const message_ = { roomname, ...message };
      this._db.putRoomMessage(message_);
      const ml = this.getRoomMessages(roomname);
      ml.putMessage(toUiMessage(message_));
    }
  }

  public async refreshRoomMessages(roomname: string) {
    await Promise.all([this.preloadRoomMessages(roomname), this.fetchRoomMessages(roomname)]);
  }

  // Loading for DMs
  public async preloadDirectMessages(
    conversation: string,
    query: MessageQuery = DEFAULT_MESSAGE_QUERY
  ) {
    const messages = await this._db.getDirectMessages(conversation, query.start, query.end);
    const ml = this.getDirectMessages(conversation);
    for (const message of messages) {
      ml.putMessage(toUiMessage(message));
    }
  }

  public async fetchDirectMessages(
    conversation: string,
    query: MessageQuery = DEFAULT_MESSAGE_QUERY
  ) {
    const messages = await this._session.call(endpoints.getDirectMessages, {
      username: conversation,
      ...query,
    });
    for (const message of messages) {
      const message_ = {
        conversation,
        msgid: message.msgid,
        author: message.src,
        enc: message.enc,
        body: message.body,
        sent: message.sent,
        version: message.version,
      };
      this._db.putDirectMessage(message_);
      const ml = this.getDirectMessages(conversation);
      ml.putMessage(toUiMessage(message_));
    }
  }

  public async refreshDirectMessages(
    username: string,
    query: MessageQuery = DEFAULT_MESSAGE_QUERY
  ) {
    await Promise.all([
      this.preloadDirectMessages(username, query),
      this.fetchDirectMessages(username, query),
    ]);
  }

  public markDirectMessagesStale() {
    for (const conversation of this._directMessages.keys()) {
      this.refreshDirectMessages(conversation);
      // TODO: be more lazy about it
    }
  }

  // Handle events
  private _handleDirectMessageNew(msg: RawDirectMessage) {
    const conversation = msg.src === this._session.myUsername ? msg.dst : msg.src;
    const internalMsg = {
      conversation,
      msgid: msg.msgid,
      author: msg.src,
      enc: msg.enc,
      body: msg.body,
      version: msg.version,
      sent: msg.sent,
    };
    this._db.putDirectMessage(internalMsg);
    const ml = this.getDirectMessages(conversation);
    ml.putMessage(toUiMessage(internalMsg));
  }

  private _handleDirectMessageEdit(
    src: string,
    dst: string,
    msgid: string,
    newVersion: number,
    newEnc: string,
    newBody: string
  ) {
    const conversation = src === this._session.myUsername ? dst : src;
    this._db.editDirectMessage(conversation, msgid, newVersion, newBody);
    const ml = this.getDirectMessages(conversation);
    const old = ml.getMessage(msgid);
    if (old && newVersion > old.version) {
      const msg = {
        conversation,
        msgid: old.msgid,
        author: old.author,
        enc: newEnc,
        body: newBody,
        version: newVersion,
        sent: old.sent,
      };
      ml.putMessage(toUiMessage(msg));
    }
  }

  private _handleDirectMessageDelete(src: string, dst: string, msgid: string, lastVersion: number) {
    const conversation = src === this._session.myUsername ? dst : src;
    this._db.deleteDirectMessage(conversation, msgid, lastVersion);
    const ml = this.getDirectMessages(conversation);
    const old = ml.getMessage(msgid);
    if (old && lastVersion > old.version) {
      const msg = {
        conversation,
        msgid: old.msgid,
        author: old.author,
        enc: null,
        body: null,
        version: lastVersion,
        sent: old.sent,
      };
      ml.putMessage(toUiMessage(msg));
    }
  }

  private _handleRoomMessageNew(msg: RoomMessage) {
    this._db.putRoomMessage({
      msgid: msg.msgid,
      roomname: msg.roomname,
      author: msg.author,
      body: msg.body,
      version: msg.version,
      sent: msg.sent,
    });
    const ml = this.getRoomMessages(msg.roomname);
    ml.putMessage(toUiMessage(msg));
  }

  private _handleRoomMessageEdit(
    roomname: string,
    msgid: string,
    newVersion: number,
    newBody: string
  ) {
    this._db.editRoomMessage(roomname, msgid, newVersion, newBody);
    const ml = this.getRoomMessages(roomname);
    const old = ml.getMessage(msgid);
    if (old && newVersion > old.version) {
      const msg = {
        roomname,
        msgid: old.msgid,
        author: old.author,
        body: newBody,
        version: newVersion,
        sent: old.sent,
      };
      ml.putMessage(toUiMessage(msg));
    }
  }

  private _handleRoomMessageDelete(roomname: string, msgid: string, lastVersion: number) {
    this._db.deleteRoomMessage(roomname, msgid, lastVersion);
    const ml = this.getRoomMessages(roomname);
    const old = ml.getMessage(msgid);
    if (old && lastVersion > old.version) {
      const msg = {
        roomname,
        msgid: old.msgid,
        author: old.author,
        body: null,
        version: lastVersion,
        sent: old.sent,
      };
      ml.putMessage(toUiMessage(msg));
    }
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    switch (evt.type) {
      case "room-message-new":
        this._handleRoomMessageNew({
          roomname: evt.roomname,
          msgid: evt.msgid,
          author: evt.author,
          body: evt.body,
          version: evt.version,
          sent: evt.time,
        });
        break;
      case "room-message-edit":
        this._handleRoomMessageEdit(evt.roomname, evt.msgid, evt.version, evt.body);
        break;
      case "room-message-delete":
        this._handleRoomMessageDelete(evt.roomname, evt.msgid, evt.version);
        break;
      case "direct-message-new":
        this._handleDirectMessageNew({
          msgid: evt.msgid,
          src: evt.src,
          dst: evt.dst,
          enc: evt.enc,
          body: evt.body,
          version: evt.version,
          sent: evt.time,
        });
        break;
      case "direct-message-edit":
        this._handleDirectMessageEdit(evt.src, evt.dst, evt.msgid, evt.version, evt.enc, evt.body);
        break;
      case "direct-message-delete":
        this._handleDirectMessageDelete(evt.src, evt.dst, evt.msgid, evt.version);
        break;
    }
  }
}
