import { Writable, Readable, writable } from "svelte/store";

import { endpoints } from "../service/endpoints";

import { TypeOf } from "../util/decoder";

import { Node, parseBbCode, minimalBbCodeConfig, fullBbCodeConfig } from "../util/body";

import { serverEventT, putRoomInfoT } from "../service/data";

import Session from "../service/session";

import { DatabaseService } from "./database";

function toUiRoomInfo(roomInfo: RoomInfo): UiRoomInfo {
  return {
    ...roomInfo,
    exists: true,
    parsedDisplay: parseBbCode(roomInfo.display, minimalBbCodeConfig),
    parsedBio: roomInfo.bio ? parseBbCode(roomInfo.bio, fullBbCodeConfig) : null,
  };
}

interface RoomInfo {
  display: string;
  bio: string | null;
  nameColor: string | null;
  backgroundColor: string | null;
}

interface UiRoomInfo extends RoomInfo {
  exists: boolean;
  parsedDisplay: Node[];
  parsedBio: Node[] | null;
}

interface RoomRole {
  username: string;
  role: string;
}

interface RoomSearchRecord {
  roomname: string;
  membersOnly: boolean;
}

function defaultUiRoomInfo(roomname: string): UiRoomInfo {
  return {
    display: roomname,
    bio: null,
    nameColor: null,
    backgroundColor: null,

    // extra
    exists: false,
    parsedDisplay: parseBbCode(roomname, minimalBbCodeConfig),
    parsedBio: null,
  };
}

export class RoomsService {
  private _db: DatabaseService;
  private _session: Session;

  private _roomInfoStores: Map<string, Writable<UiRoomInfo>>;
  private _avatarUrlStores: Map<string, Writable<string>>;
  private _roleListStores: Map<string, Writable<RoomRole[]>>;
  private _myRoleStores: Map<string, Writable<string>>;

  public constructor(db: DatabaseService, session: Session) {
    this._db = db;
    this._session = session;
    this._roomInfoStores = new Map();
    this._avatarUrlStores = new Map();
    this._roleListStores = new Map();
    this._myRoleStores = new Map();
  }

  public getRoomInfoStore(roomname: string): Readable<RoomInfo> {
    let store = this._roomInfoStores.get(roomname);
    if (!store) {
      store = writable(defaultUiRoomInfo(roomname));
      this._roomInfoStores.set(roomname, store);
      this.preloadRoomInfo(roomname);
    }
    return store;
  }

  public getAvatarUrlStore(roomname: string): Readable<string> {
    let store = this._avatarUrlStores.get(roomname);
    if (!store) {
      store = writable(this.roomAvatarUrl(roomname) + "?t=" + Date.now());
      this._avatarUrlStores.set(roomname, store);
    }
    return store;
  }

  public getRolesListStore(roomname: string): Readable<RoomRole[]> {
    let store = this._roleListStores.get(roomname);
    if (!store) {
      store = writable([]);
      this._roleListStores.set(roomname, store);
      this._fetchRoleList(roomname);
    }
    return store;
  }

  public getMyRoleStore(roomname: string): Readable<string> {
    let store = this._myRoleStores.get(roomname);
    if (!store) {
      store = writable("none");
      this._myRoleStores.set(roomname, store);
      this._fetchMyRole(roomname);
    }
    return store;
  }

  public async search(name: string): Promise<RoomSearchRecord[]> {
    const res = await this._session.call(endpoints.getSearchRooms, {
      name,
    });
    return res.rooms;
  }

  public roomAvatarUrl(roomname: string): string {
    return "/data/room-avatar/" + roomname + ".png";
  }

  public async uploadAvatar(roomname: string, file: File) {
    await fetch("/api/rooms/" + roomname + "/avatar", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
  }

  public async putRoomInfo(roomname: string, roomInfo: TypeOf<typeof putRoomInfoT>) {
    await this._session.call(endpoints.putRoomInfo, {
      roomname,
      roomInfo,
    });
  }

  public async createRoom(roomname: string, membersOnly: boolean) {
    return await this._session.call(endpoints.postRoom, {
      roomname,
      membersOnly,
    });
  }

  public async setUserRole(roomname: string, username: string, role: string) {
    return await this._session.call(endpoints.putRoomRole, {
      roomname,
      username,
      role,
    });
  }

  public async sendTick(dst: string, code: string) {
    await this._session.call(endpoints.postTick, {
      dst,
      code,
    });
  }

  private async _fetchRoomInfo(roomname: string) {
    let store = this._roomInfoStores.get(roomname);
    if (!store) {
      store = writable(defaultUiRoomInfo(roomname));
      this._roomInfoStores.set(roomname, store);
    }
    const remoteRoomInfoPromise = this._session.call(endpoints.getRoomInfo, { roomname });
    const localRoomInfo = await this._db.getRoomInfo(roomname);
    if (localRoomInfo) {
      store.set(toUiRoomInfo(localRoomInfo));
    }
    const remoteRoomInfo = await remoteRoomInfoPromise;
    store.set(toUiRoomInfo(remoteRoomInfo));
    this._db.putRoomInfo({ roomname, ...remoteRoomInfo });
  }

  public async preloadRoomInfo(roomname: string) {
    let store = this._roomInfoStores.get(roomname);
    if (!store) {
      store = writable(defaultUiRoomInfo(roomname));
      this._roomInfoStores.set(roomname, store);
    }
    const localRoomInfo = await this._db.getRoomInfo(roomname);
    if (localRoomInfo) {
      store.set(toUiRoomInfo(localRoomInfo));
    }
  }

  public async refreshRoomInfo(roomname: string) {
    await this._fetchRoomInfo(roomname);
    const store = this._avatarUrlStores.get(roomname);
    if (store) {
      store.set(this.roomAvatarUrl(roomname) + "?t=" + Date.now());
    }
  }

  private async _fetchRoleList(roomname: string) {
    const res = await this._session.call(endpoints.getRoomRoles, { roomname });
    let store = this._roleListStores.get(roomname);
    if (!store) {
      store = writable([]);
      this._roleListStores.set(roomname, store);
    }
    store.set(res.roles);
  }

  public async refreshRoomRoles(roomname: string) {
    await this._fetchRoleList(roomname);
  }

  private async _fetchMyRole(roomname: string) {
    const res = await this._session.call(endpoints.getRoomRole, {
      roomname,
      username: this._session.myUsername,
    });
    let store = this._myRoleStores.get(roomname);
    if (!store) {
      store = writable("none");
      this._myRoleStores.set(roomname, store);
    }
    store.set(res.role);
  }

  public refreshRoomAvatar(roomname: string) {
    const store = this._avatarUrlStores.get(roomname);
    if (store) {
      store.set(this.roomAvatarUrl(roomname) + "?t=" + Date.now());
    }
  }

  private _handleRoomAvatarUpdate(roomname: string) {
    const store = this._avatarUrlStores.get(roomname);
    if (store) {
      store.set(this.roomAvatarUrl(roomname) + "?t=" + Date.now());
    }
  }

  private _handleRoomInfoUpdate(roomname: string) {
    const store = this._roomInfoStores.get(roomname);
    if (store) {
      // useless if nobody cares about the value
      this._fetchRoomInfo(roomname);
    }
  }

  private _handleRoomRoleUpdate(roomname: string, username: string, role: string) {
    if (username === this._session.myUsername) {
      const store = this._myRoleStores.get(roomname);
      if (store) {
        // useless if nobody cares about the value
        store.set(role);
      }
    }
    const store = this._roleListStores.get(roomname);
    if (store) {
      // again, useless if nobody cares about the value
      store.update((rl) => {
        let foundIdx = null;
        for (let i = 0; i < rl.length; i++) {
          const r = rl[i];
          if (r.username === username) {
            r.role = role;
            foundIdx = i;
            break;
          }
        }
        if (foundIdx !== null && role === "none") {
          rl.splice(foundIdx, 1);
        } else if (foundIdx === null) {
          rl.push({
            username,
            role,
          });
        }
        return rl;
      });
    }
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    let store;
    switch (evt.type) {
      case "room-avatar-update":
        this._handleRoomAvatarUpdate(evt.roomname);
        break;
      case "room-info-update":
        this._handleRoomInfoUpdate(evt.roomname);
        break;
      case "room-role-update":
        this._handleRoomRoleUpdate(evt.roomname, evt.username, evt.role);
        break;
    }
  }
}
