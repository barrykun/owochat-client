import { TypeOf } from "../util/decoder";
import { serverEventT } from "../service/data";
import Session from "../service/session";

import { Subscriber } from "./subscriber";
import { EmojiService } from "./emoji";
import { MessagesService } from "./messages";
import { UsersService } from "./users";
import { RoomsService } from "./rooms";
import { UpdatesService } from "./updates";
import { UploadsService } from "./uploads";
import { NotifyService } from "./notify";
import { SettingsService } from "./settings";
import { DatabaseService } from "./database";
import { ConversationsService } from "./conversations";

import MY_COMMIT_HASH from "../my-commit-hash";

class RestrictedServices {
  public readonly isRestricted: boolean;

  public readonly updates: UpdatesService;

  public constructor() {
    this.isRestricted = true;
    this.updates = new UpdatesService(MY_COMMIT_HASH);
  }
}

class Services {
  public readonly isRestricted: boolean;

  public readonly subscriber: Subscriber;
  public readonly database: DatabaseService;
  public readonly emoji: EmojiService;
  public readonly messages: MessagesService;
  public readonly users: UsersService;
  public readonly rooms: RoomsService;
  public readonly updates: UpdatesService;
  public readonly uploads: UploadsService;
  public readonly notify: NotifyService;
  public readonly settings: SettingsService;
  public readonly conversations: ConversationsService;

  public constructor(session: Session) {
    this.isRestricted = false;
    this.subscriber = new Subscriber(session, {
      onEvent: this._onEvent.bind(this),
      onListen: this._onListen.bind(this),
      onUnlisten: this._onUnlisten.bind(this),
      onStale: this._onStale.bind(this),
      onReady: this._onReady.bind(this),
      onDisconnect: this._onDisconnect.bind(this),
      onDenied: this._onDenied.bind(this),
    });
    this.database = new DatabaseService(session);
    this.emoji = new EmojiService(session);
    this.messages = new MessagesService(this.database, this.subscriber, session);
    this.users = new UsersService(this.database, session);
    this.rooms = new RoomsService(this.database, session);
    this.updates = new UpdatesService(MY_COMMIT_HASH);
    this.uploads = new UploadsService(session);
    this.notify = new NotifyService(session);
    this.settings = new SettingsService(session);
    this.conversations = new ConversationsService(this.database, session);
    this.subscriber.connect();
  }

  private _onEvent(evt: TypeOf<typeof serverEventT>) {
    console.log("onEvent:", evt);
    this.emoji.handleEvent(evt);
    this.messages.handleEvent(evt);
    this.users.handleEvent(evt);
    this.rooms.handleEvent(evt);
    this.updates.handleEvent(evt);
    this.notify.handleEvent(evt);
    this.conversations.handleEvent(evt);
  }

  private _onListen(channel: string) {
    console.log("onListen:", channel);
    const sp = channel.split(":");
    if (sp.length === 2) {
      if (sp[0] === "room") {
        this.messages.refreshRoomMessages(sp[1]);
      } else if (sp[0] === "user-info") {
        this.users.refreshUserInfo(sp[1]);
      } else if (sp[0] === "room-info") {
        this.rooms.refreshRoomInfo(sp[1]);
        this.rooms.refreshRoomAvatar(sp[1]);
      }
    }
  }

  private _onUnlisten(channel: string) {
    console.log("onUnlisten:", channel);
  }

  private _onStale() {
    console.log("onStale");
    this.updates.checkVersion();
  }

  private _onReady() {
    console.log("onReady");
    this.users.onReconnect();
    this.conversations.refreshConversations();
    this.messages.markDirectMessagesStale();
  }

  private _onDisconnect() {
    console.log("onDisconnect");
  }

  private _onDenied() {
    console.log("onDenied");
  }
}

let restrictedServices: RestrictedServices | null = null;

const servicesMap = new Map();

function servicesFor(session: Session): Services;
function servicesFor(session: null): RestrictedServices;
function servicesFor(session: Session | null): Services | RestrictedServices {
  if (session) {
    let services = servicesMap.get(session.authorization);
    if (!services) {
      services = new Services(session);
      servicesMap.set(session.authorization, services);
    }
    return services;
  } else {
    if (!restrictedServices) {
      restrictedServices = new RestrictedServices();
    }
    return restrictedServices;
  }
}

export default servicesFor;
