import Session from "../service/session";

import { Writable, Readable, writable, derived } from "svelte/store";

import { endpoints } from "../service/endpoints";

import { TypeOf } from "../util/decoder";

import { Node, parseBbCode, minimalBbCodeConfig, fullBbCodeConfig } from "../util/body";

import { serverEventT, userInfoT } from "../service/data";

import { DatabaseService } from "./database";

const STRANGER_AVATAR_URL = "/app/stranger.png";

function toUiUserInfo(userInfo: UserInfo): UiUserInfo {
  return {
    ...userInfo,
    exists: true,
    parsedDisplay: parseBbCode(userInfo.display, minimalBbCodeConfig),
    parsedBio: userInfo.bio ? parseBbCode(userInfo.bio, fullBbCodeConfig) : null,
  };
}

type Friend = {
  username: string;
};

type UserInfoExtraField = {
  field: string;
  value: string;
};

export type Alignment = "top-left" | "top-right" | "bottom-left" | "bottom-right" | "center";

export type Repeat = "no-repeat" | "repeat-x" | "repeat-y" | "repeat-xy";

export interface UserInfo {
  display: string;

  age: string | null;
  gender: string | null;
  location: string | null;

  bio: string | null;

  extraFields: UserInfoExtraField[];

  nameColor: string | null;
  textColor: string | null;

  bannerColor: string | null;
  buttonColor: string | null;

  messageBackground: UserInfoBackground;
  profileBackground: UserInfoBackground;

  avatarSetAt: Date | null;

  messageBackgroundSetAt: Date | null;
  profileBackgroundSetAt: Date | null;
}

export interface UserInfoBackground {
  enabled: boolean;
  image: boolean;
  alpha: number;
  align: Alignment;
  color: string | null;
  repeat: Repeat;
  size: string;
}

export interface UiUserInfo extends UserInfo {
  exists: boolean;
  parsedDisplay: Node[];
  parsedBio: Node[] | null;
}

interface UserSearchRecord {
  username: string;
}

function defaultBackground(): UserInfoBackground {
  return {
    enabled: false,
    image: false,
    alpha: 100,
    align: "top-right",
    color: null,
    repeat: "no-repeat",
    size: "auto auto",
  };
}

function defaultUiUserInfo(username: string): UiUserInfo {
  return {
    display: username,

    age: null,
    gender: null,
    location: null,

    bio: null,

    extraFields: [],

    nameColor: null,
    textColor: null,

    bannerColor: null,
    buttonColor: null,

    messageBackground: defaultBackground(),
    profileBackground: defaultBackground(),

    avatarSetAt: null,

    messageBackgroundSetAt: null,
    profileBackgroundSetAt: null,

    // extra
    exists: false,
    parsedDisplay: parseBbCode(username, minimalBbCodeConfig),
    parsedBio: null,
  };
}

export class UsersService {
  private _session: Session;

  private _userInfoStores: Map<string, Writable<UiUserInfo>>;

  private _starredRoomsStores: Map<string, Writable<{ roomname: string; notifyMode: string }[]>>;

  private _myFriendsStore: Writable<Friend[]>;

  private _db: DatabaseService;

  public constructor(db: DatabaseService, session: Session) {
    this._db = db;
    this._session = session;
    this._userInfoStores = new Map();
    this._starredRoomsStores = new Map();
    this._myFriendsStore = writable([]);
    this._fetchMyFriends();
  }

  public getUserInfoStore(username: string): Readable<UiUserInfo> {
    let store = this._userInfoStores.get(username);
    if (!store) {
      store = writable(defaultUiUserInfo(username));
      this._userInfoStores.set(username, store);
      this.preloadUserInfo(username);
    }
    return store;
  }

  public getAvatarUrlStore(username: string): Readable<string> {
    return derived(this.getUserInfoStore(username), (userInfo) =>
      userInfo.avatarSetAt
        ? this.userAvatarUrlAt(username, userInfo.avatarSetAt)
        : STRANGER_AVATAR_URL
    );
  }

  public getMessageBackgroundUrlStore(username: string): Readable<string | null> {
    return derived(this.getUserInfoStore(username), (userInfo) =>
      userInfo.messageBackgroundSetAt
        ? this.userMessageBackgroundUrlAt(username, userInfo.messageBackgroundSetAt)
        : null
    );
  }

  public getProfileBackgroundUrlStore(username: string): Readable<string | null> {
    return derived(this.getUserInfoStore(username), (userInfo) =>
      userInfo.profileBackgroundSetAt
        ? this.userProfileBackgroundUrlAt(username, userInfo.profileBackgroundSetAt)
        : null
    );
  }

  public getStarredRoomsStore(
    username: string
  ): Readable<{ roomname: string; notifyMode: string }[]> {
    let store = this._starredRoomsStores.get(username);
    if (!store) {
      store = writable([]);
      this._starredRoomsStores.set(username, store);
      this._fetchStarredRooms(username);
    }
    return store;
  }

  public getMyFriendsStore(): Readable<Friend[]> {
    return this._myFriendsStore;
  }

  public userAvatarUrlAt(username: string, at: Date): string {
    return "/data/user-avatar/" + username + ".png?t=" + at.valueOf();
  }

  public userMessageBackgroundUrlAt(username: string, at: Date): string {
    return "/data/user-background/" + username + ".png?t=" + at.valueOf();
  }

  public userProfileBackgroundUrlAt(username: string, at: Date): string {
    return "/data/profile-background/" + username + ".png?t=" + at.valueOf();
  }

  public async search(name: string): Promise<UserSearchRecord[]> {
    const res = await this._session.call(endpoints.getSearchUsers, {
      name,
    });
    return res.users;
  }

  public async setUserInfo(username: string, userInfo: TypeOf<typeof userInfoT>) {
    await this._session.call(endpoints.putUserInfo, {
      username,
      userInfo,
    });
  }

  public async uploadAvatar(username: string, file: File) {
    await fetch("/api/users/" + username + "/avatar", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
  }

  public async uploadMessageBackground(username: string, file: File) {
    await fetch("/api/users/" + username + "/message-background", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
  }

  public async uploadProfileBackground(username: string, file: File) {
    await fetch("/api/users/" + username + "/profile-background", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
  }

  public async addUserStarredRoom(username: string, roomname: string, notifyMode: string) {
    await this._session.call(endpoints.putUserStarredRoom, {
      username,
      roomname,
      notifyMode,
    });
  }

  public async removeUserStarredRoom(username: string, roomname: string) {
    await this._session.call(endpoints.deleteUserStarredRoom, {
      username,
      roomname,
    });
  }

  public async addFriend(friend: string) {
    await this._session.call(endpoints.putUserFriend, {
      username: this._session.myUsername,
      friend,
    });
  }

  public async removeFriend(friend: string) {
    await this._session.call(endpoints.deleteUserFriend, {
      username: this._session.myUsername,
      friend,
    });
  }

  private async _fetchUserInfo(username: string) {
    let store = this._userInfoStores.get(username);
    if (!store) {
      store = writable(defaultUiUserInfo(username));
      this._userInfoStores.set(username, store);
    }
    const remoteUserInfoPromise = this._session.call(endpoints.getUserInfo, { username });
    const localUserInfo = await this._db.getUserInfo(username);
    if (localUserInfo) {
      store.set(toUiUserInfo(localUserInfo));
    }
    const userInfo = await remoteUserInfoPromise;
    await this._db.putUserInfo({ username, ...userInfo });
    store.set(toUiUserInfo(userInfo));
  }

  public async preloadUserInfo(username: string) {
    let store = this._userInfoStores.get(username);
    if (!store) {
      store = writable(defaultUiUserInfo(username));
      this._userInfoStores.set(username, store);
    }
    const localUserInfo = await this._db.getUserInfo(username);
    if (localUserInfo) {
      store.set(toUiUserInfo(localUserInfo));
    }
  }

  public async refreshUserInfo(username: string) {
    await this._fetchUserInfo(username);
  }

  private async _fetchStarredRooms(username: string) {
    const resPromise = this._session.call(endpoints.getUserStarredRooms, {
      username,
    });
    let store = this._starredRoomsStores.get(username);
    if (!store) {
      store = writable([]);
      this._starredRoomsStores.set(username, store);
    }
    const localStarredRooms = await this._db.getStarredRooms();
    store.set(localStarredRooms);
    const res = await resPromise;
    res.rooms.sort((a, b) => a.roomname.localeCompare(b.roomname));
    store.set(res.rooms);
    this._db.updateStarredRooms(res.rooms);
  }

  private async _fetchMyFriends() {
    const resPromise = this._session.call(endpoints.getUserFriends, {
      username: this._session.myUsername,
    });
    const dbFriends = await this._db.getFriends();
    this._myFriendsStore.set(dbFriends);
    const remoteFriends = await resPromise;
    this._myFriendsStore.set(remoteFriends.friends);
    this._db.updateFriends(remoteFriends.friends);
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    let store;
    switch (evt.type) {
      case "user-info-update":
        this._fetchUserInfo(evt.username);
        break;
      case "user-starred-add":
        this._fetchStarredRooms(evt.username);
        // TODO: more fine-grained behavior
        break;
      case "user-starred-remove":
        this._fetchStarredRooms(evt.username);
        // TODO: more fine-grained behavior
        break;
      case "user-friend-add":
        if (evt.src === this._session.myUsername) {
          this._db.putFriend({
            username: evt.dst,
          });
          this._myFriendsStore.update((friends) => {
            friends = friends.filter((friend) => friend.username != evt.dst);
            friends.push({
              username: evt.dst,
            });
            return friends;
          });
        }
        break;
      case "user-friend-remove":
        if (evt.src === this._session.myUsername) {
          this._db.deleteFriend(evt.dst);
          this._myFriendsStore.update((friends) => {
            return friends.filter((friend) => friend.username != evt.dst);
          });
        }
        break;
    }
  }

  public onReconnect() {
    this._fetchMyFriends();
    // TODO: re-fetch starred rooms
  }
}
