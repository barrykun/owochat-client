import { endpoints } from "../service/endpoints";

import { writable, Writable, Readable } from "svelte/store";

import { parseBbCode, fullBbCodeConfig, nodesToPlain } from "../util/body";

import { TypeOf } from "../util/decoder";
import { serverEventT, notificationT } from "../service/data";
import Session from "../service/session";

import base64 from "base64-arraybuffer";

export type Notification =
  | {
      type: "mention";
      notifid: string;
      dst: string;
      createdAt: Date;
      username: string;
      roomname: string;
      body: string;
    }
  | {
      type: "room-message";
      notifid: string;
      dst: string;
      createdAt: Date;
      username: string;
      roomname: string;
      body: string;
    }
  | {
      type: "direct-message";
      notifid: string;
      dst: string;
      createdAt: Date;
      username: string;
      body: string;
    };

export class NotificationsList {
  private _list: Notification[];
  private _store: Writable<Notification[]>;
  private _lastRead: string | null;
  private _lastReadStore: Writable<string | null>;
  private _unreadStore: Writable<Notification[]>;

  public constructor() {
    this._list = [];
    this._store = writable(this._list);
    this._lastRead = null;
    this._lastReadStore = writable(null);
    this._unreadStore = writable(this._list);
  }

  public getStore(): Readable<Notification[]> {
    return this._store;
  }

  public getUnreadStore(): Readable<Notification[]> {
    return this._unreadStore;
  }

  public put(newNotif: Notification) {
    let mustAppend = true;
    for (let i = 0; i < this._list.length; i++) {
      const notif = this._list[i];
      if (newNotif.notifid < notif.notifid) {
        this._list.splice(i, 0, newNotif);
        mustAppend = false;
        break;
      } else if (newNotif.notifid === notif.notifid) {
        this._list.splice(i, 1, newNotif);
        mustAppend = false;
        break;
      }
    }
    if (mustAppend) {
      this._list.push(newNotif);
    }
    this._updateUnread();
  }

  public readUntil(notifid: string) {
    if (!this._lastRead || notifid > this._lastRead) {
      this._lastRead = notifid;
      this._lastReadStore.set(notifid);
    }
    this._updateUnread();
  }

  public markAllRead() {
    if (this._list.length > 0) {
      this.readUntil(this._list[this._list.length - 1].notifid);
    }
  }

  public _updateUnread() {
    const lastRead = this._lastRead;
    if (lastRead) {
      this._unreadStore.set(this._list.filter((n) => n.notifid > lastRead));
    } else {
      this._unreadStore.set(this._list);
    }
  }
}

type WebpushInfo = { endpoint: string; keys: { p256dh: string; auth: string } };

type Conversation = { type: "room"; roomname: string } | { type: "dm"; conversation: string };

export class NotifyService {
  private _session: Session;
  private _currentConversation: Conversation | null;
  private _notifications: NotificationsList;
  private _notificationsStores: Map<string, Writable<number>>;
  private _enabled: boolean;

  public constructor(session: Session) {
    this._session = session;
    this._currentConversation = null;
    this._notifications = new NotificationsList();
    this._notificationsStores = new Map();
    this._enabled =
      localStorage.getItem(this._session.myUsername + ":notifications-enabled") === "true";
  }

  public isEnabled() {
    return this._enabled;
  }

  public setEnabled(enabled: boolean) {
    this._enabled = enabled;
    if (enabled) {
      localStorage.setItem(this._session.myUsername + ":notifications-enabled", "true");
      this._updatePush();
    } else {
      localStorage.removeItem(this._session.myUsername + ":notifications-enabled");
      this._disablePush();
    }
  }

  private async _updatePush() {
    const reg = await navigator.serviceWorker.getRegistration();
    if (reg) {
      const pm = reg.pushManager;
      const permissionState = await pm.permissionState();
      if (permissionState === "granted") {
        const config = await this._session.call(endpoints.getConfig, {});
        const applicationServerKey = base64.decode(
          config.vapidPubkey.replace(/_/g, "/").replace(/-/g, "+")
        );
        const sub = await pm.subscribe({
          userVisibleOnly: true,
          applicationServerKey,
        });
        const subJson = sub.toJSON() as WebpushInfo;
        await this._session.call(endpoints.putUserDevicePush, {
          username: this._session.myUsername,
          deviceToken: this._session.deviceToken,
          data: subJson,
        });
      }
    }
  }

  private async _disablePush() {
    await this._session.call(endpoints.deleteUserDevicePush, {
      username: this._session.myUsername,
      deviceToken: this._session.deviceToken,
    });
  }

  public markAllRead() {
    this._notifications.markAllRead();
  }

  public getNotifications(): NotificationsList {
    return this._notifications;
  }

  public getRoomNotificationsStore(roomname: string): Readable<number> {
    const key = "room:" + roomname;
    let store = this._notificationsStores.get(key);
    if (!store) {
      store = writable(0);
      this._notificationsStores.set(key, store);
    }
    return store;
  }

  public getDmNotificationsStore(conversation: string): Readable<number> {
    const key = "dm:" + conversation;
    let store = this._notificationsStores.get(key);
    if (!store) {
      store = writable(0);
      this._notificationsStores.set(key, store);
    }
    return store;
  }

  public setCurrentConversation(conversation: Conversation | null) {
    let store;
    this._currentConversation = conversation;
    if (conversation) {
      switch (conversation.type) {
        case "room":
          store = this._notificationsStores.get("room:" + conversation.roomname);
          if (store) {
            store.set(0);
          }
          break;
        case "dm":
          store = this._notificationsStores.get("dm:" + conversation.conversation);
          if (store) {
            store.set(0);
          }
          break;
      }
    }
  }

  private _processNotificationAdd(notification: TypeOf<typeof notificationT>) {
    switch (notification.type) {
      case "mention":
      case "room-message":
        this._notifications.put({
          type: notification.type,
          notifid: notification.notifid,
          dst: notification.dst,
          createdAt: notification.createdAt,
          username: notification.username,
          roomname: notification.roomname,
          body: notification.body,
        });
        break;
      case "direct-message":
        this._notifications.put({
          type: notification.type,
          notifid: notification.notifid,
          dst: notification.dst,
          createdAt: notification.createdAt,
          username: notification.username,
          body: notification.body,
        });
        break;
    }
    if (document.hasFocus()) {
      this._notifications.markAllRead();
    }
    let chatFocused = false;
    let ckey = "invalid";
    if (this._currentConversation && document.hasFocus()) {
      switch (notification.type) {
        case "mention":
        case "room-message":
          if (this._currentConversation.type === "room") {
            chatFocused = this._currentConversation.roomname === notification.roomname;
            ckey = "room:" + this._currentConversation.roomname;
          }
          break;
        case "direct-message":
          if (this._currentConversation.type === "dm") {
            chatFocused = this._currentConversation.conversation === notification.username;
            ckey = "dm:" + this._currentConversation.conversation;
          }
          break;
      }
    }
    if (!chatFocused) {
      const store = this._notificationsStores.get(ckey);
      if (store) {
        store.update((c) => c + 1);
      } else {
        this._notificationsStores.set(ckey, writable(1));
      }
    }
    const bodyParsed = parseBbCode(notification.body, fullBbCodeConfig);
    switch (notification.type) {
      case "mention":
        if (this._enabled) {
          const audio = new Audio("/app/mention.wav");
          audio.play();
          if (!chatFocused) {
            new Notification(
              `mentioned by @${notification.username} in #${notification.roomname}`,
              {
                icon: "/data/user-avatar/" + notification.username + ".png?t=" + Date.now(),
                body: nodesToPlain(bodyParsed),
              }
            );
          }
        }
        break;
      case "room-message":
        if (this._enabled) {
          const audio = new Audio("/app/message.wav");
          audio.play();
          if (!chatFocused) {
            new Notification(
              `message from @${notification.username} in #${notification.roomname}`,
              {
                icon: "/data/user-avatar/" + notification.username + ".png?t=" + Date.now(),
                tag: `room-message-${notification.roomname}`,
                renotify: true,
                body: nodesToPlain(bodyParsed),
              }
            );
          }
        }
        break;
      case "direct-message":
        if (this._enabled) {
          const audio = new Audio("/app/message.wav");
          audio.play();
          if (!chatFocused) {
            new Notification(`direct message from @${notification.username}`, {
              icon: "/data/user-avatar/" + notification.username + ".png?t=" + Date.now(),
              tag: `direct-message-${notification.username}`,
              renotify: true,
              body: nodesToPlain(bodyParsed),
            });
          }
        }
        break;
    }
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    switch (evt.type) {
      case "user-notification-add":
        if (evt.notification.dst === this._session.myUsername) {
          this._processNotificationAdd(evt.notification);
        }
        break;
    }
  }
}
