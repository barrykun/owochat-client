import Dexie from "dexie";

import Session from "../service/session";

class Database extends Dexie {
  public userInfo: Dexie.Table<UserInfo, string>;

  public roomInfo: Dexie.Table<RoomInfo, string>;

  public roomMessages: Dexie.Table<RoomMessage, [string, string]>;

  public directMessages: Dexie.Table<DirectMessage, [string, string]>;

  public conversationInfo: Dexie.Table<ConversationInfo, string>;

  public starredRooms: Dexie.Table<StarredRoom, string>;

  public friends: Dexie.Table<Friend, string>;

  public constructor(myUsername: string) {
    super(myUsername + ":database");
    this.version(9)
      .stores({
        "user-info": "username",
        "room-info": "roomname",
        "room-messages": "[roomname+msgid]",
        "direct-messages": "[conversation+msgid]",
        "conversation-info": "conversation,lastActivity",
        "starred-rooms": "roomname",
        friends: "username",
      })
      .upgrade((tx) => {
        const userInfo = tx.table("user-info");
        userInfo.toCollection().modify((info) => {
          info.messageBackgroundSetAt = info.backgroundSetAt;
          info.profileBackgroundSetAt = null;
          delete info.backgroundSetAt;
        });
      });
    this.version(8)
      .stores({
        "user-info": "username",
        "room-info": "roomname",
        "room-messages": "[roomname+msgid]",
        "direct-messages": "[conversation+msgid]",
        "conversation-info": "conversation,lastActivity",
        "starred-rooms": "roomname",
        friends: "username",
      })
      .upgrade((tx) => {
        const userInfo = tx.table("user-info");
        userInfo.toCollection().modify((info) => {
          info.messageBackground = info.background;
          info.profileBackground = {
            enabled: false,
            image: false,
            alpha: 100,
            align: "top-right",
            color: null,
            repeat: "no-repeat",
            size: "auto auto",
          };
          delete info.background;
        });
      });
    this.version(7)
      .stores({
        "user-info": "username",
        "room-info": "roomname",
        "room-messages": "[roomname+msgid]",
        "direct-messages": "[conversation+msgid]",
        "conversation-info": "conversation,lastActivity",
        "starred-rooms": "roomname",
        friends: "username",
      })
      .upgrade((tx) => {
        const conversationInfo = tx.table("conversation-info");
        conversationInfo.toCollection().modify((c) => {
          c.lastMsgid = null;
          c.lastReadMsgid = null;
          c.partnerLastReadMsgid = null;
          delete c.unreadCount;
        });
      });
    this.version(6)
      .stores({
        "user-info": "username",
        "room-info": "roomname",
        "room-messages": "[roomname+msgid]",
        "direct-messages": "[conversation+msgid]",
        "conversation-info": "conversation,lastActivity",
        "starred-rooms": "roomname",
        friends: "username",
      })
      .upgrade((tx) => {
        const conversationInfo = tx.table("conversation-info");
        conversationInfo.toCollection().modify((c) => {
          c.profileOpen = true;
          c.unreadCount = 0;
        });
      });
    this.version(5).stores({
      "user-info": "username",
      "room-info": "roomname",
      "room-messages": "[roomname+msgid]",
      "direct-messages": "[conversation+msgid]",
      "conversation-info": "conversation,lastActivity",
      "starred-rooms": "roomname",
      friends: "username",
    });
    this.version(4).stores({
      "user-info": "username",
      "room-info": "roomname",
      "room-messages": "[roomname+msgid]",
      "direct-messages": "[conversation+msgid]",
      "starred-rooms": "roomname",
      friends: "username",
    });
    this.version(3)
      .stores({
        "user-info": "username",
        "room-info": "roomname",
        "room-messages": "[roomname+msgid]",
        "starred-rooms": "roomname",
        friends: "username",
      })
      .upgrade((tx) => {
        const roomInfo = tx.table("room-info");
        roomInfo.toCollection().modify((r) => {
          r.nameColor = r.color;
          r.backgroundColor = null;
          delete r.color;
        });
      });
    this.version(2).stores({
      "user-info": "username",
      "room-info": "roomname",
      "room-messages": "[roomname+msgid]",
      "starred-rooms": "roomname",
      friends: "username",
    });
    this.version(1).stores({
      "user-info": "username",
      "room-info": "roomname",
      "room-messages": "[roomname+msgid]",
    });
    this.userInfo = this.table("user-info");
    this.roomInfo = this.table("room-info");
    this.roomMessages = this.table("room-messages");
    this.directMessages = this.table("direct-messages");
    this.conversationInfo = this.table("conversation-info");
    this.starredRooms = this.table("starred-rooms");
    this.friends = this.table("friends");
  }
}

export type Alignment = "top-left" | "top-right" | "bottom-left" | "bottom-right" | "center";

export type Repeat = "no-repeat" | "repeat-x" | "repeat-y" | "repeat-xy";

export interface RoomMessage {
  msgid: string;
  roomname: string;
  author: string;
  body: string | null;
  sent: Date;
  version: number;
}

export interface DirectMessage {
  conversation: string;
  author: string;
  msgid: string;
  enc: string | null;
  body: string | null;
  sent: Date;
  version: number;
}

export interface UserInfoBackground {
  enabled: boolean;
  image: boolean;
  alpha: number;
  align: Alignment;
  color: string | null;
  repeat: Repeat;
  size: string;
}

export interface UserInfoExtraField {
  field: string;
  value: string;
}

export interface UserInfo {
  username: string;

  display: string;

  age: string | null;
  gender: string | null;
  location: string | null;

  bio: string | null;

  extraFields: UserInfoExtraField[];

  nameColor: string | null;
  textColor: string | null;

  bannerColor: string | null;
  buttonColor: string | null;

  messageBackground: UserInfoBackground;
  profileBackground: UserInfoBackground;

  avatarSetAt: Date | null;

  messageBackgroundSetAt: Date | null;
  profileBackgroundSetAt: Date | null;

  updatedAt: Date;
}

export interface RoomInfo {
  roomname: string;

  display: string;
  bio: string | null;

  nameColor: string | null;
  backgroundColor: string | null;

  updatedAt: Date;
}

export interface ConversationInfo {
  conversation: string;
  opened: boolean;
  profileOpen: boolean;
  lastMsgid: string | null;
  lastReadMsgid: string | null;
  partnerLastReadMsgid: string | null;
  lastActivity: Date | null;
}

export interface StarredRoom {
  roomname: string;
  notifyMode: string;
}

export interface Friend {
  username: string;
}

const MSGID_MIN = "000000000000000000000000";
const MSGID_MAX = "ffffffffffffffffffffffff";

export class DatabaseService {
  private _session: Session;
  private _db: Database;

  public constructor(session: Session) {
    this._session = session;
    this._db = new Database(session.myUsername);
  }

  public async getUserInfo(username: string): Promise<UserInfo | null> {
    return (await this._db.userInfo.get(username)) || null;
  }

  public async getRoomInfo(roomname: string): Promise<RoomInfo | null> {
    return (await this._db.roomInfo.get(roomname)) || null;
  }

  public async getStarredRooms(): Promise<StarredRoom[]> {
    return await this._db.starredRooms.toArray();
  }

  public async getFriends(): Promise<Friend[]> {
    return await this._db.friends.toArray();
  }

  public async getDirectMessages(
    conversation: string,
    start: string | null,
    end: string | null
  ): Promise<DirectMessage[]> {
    const realStart = start || MSGID_MIN;
    const realEnd = end || MSGID_MAX;
    const messages = await this._db.directMessages
      .where("[conversation+msgid]")
      .between([conversation, realStart], [conversation, realEnd])
      .reverse()
      .limit(50)
      .toArray();
    messages.reverse();
    return messages;
  }

  public async getRoomMessages(
    roomname: string,
    start: string | null,
    end: string | null
  ): Promise<RoomMessage[]> {
    const realStart = start || MSGID_MIN;
    const realEnd = end || MSGID_MAX;
    const messages = await this._db.roomMessages
      .where("[roomname+msgid]")
      .between([roomname, realStart], [roomname, realEnd])
      .reverse()
      .limit(50)
      .toArray();
    messages.reverse();
    return messages;
  }

  public async getConversationInfo(conversation: string) {
    const info = await this._db.conversationInfo.get(conversation);
    return info;
  }

  public async getAllConversations(): Promise<ConversationInfo[]> {
    const conversations = await this._db.conversationInfo.orderBy("lastActivity").toArray();
    return conversations;
  }

  public async putUserInfo(userInfo: UserInfo) {
    await this._db.transaction("rw", this._db.userInfo, async () => {
      const dbUserInfo = await this._db.userInfo.get(userInfo.username);
      if (!dbUserInfo || dbUserInfo.updatedAt < userInfo.updatedAt) {
        await this._db.userInfo.put(userInfo);
      }
    });
  }

  public async putRoomInfo(roomInfo: RoomInfo) {
    await this._db.transaction("rw", this._db.roomInfo, async () => {
      const dbRoomInfo = await this._db.roomInfo.get(roomInfo.roomname);
      if (!dbRoomInfo || dbRoomInfo.updatedAt < roomInfo.updatedAt) {
        await this._db.roomInfo.put(roomInfo);
      }
    });
  }

  public async putDirectMessage(msg: DirectMessage) {
    await this._db.transaction("rw", this._db.directMessages, async () => {
      const dbMessage = await this._db.directMessages.get([msg.conversation, msg.msgid]);
      if (!dbMessage || dbMessage.version < msg.version) {
        await this._db.directMessages.put(msg);
      }
    });
  }

  public async editDirectMessage(
    conversation: string,
    msgid: string,
    newVersion: number,
    newBody: string
  ) {
    await this._db.transaction("rw", this._db.directMessages, async () => {
      const dbMessage = await this._db.directMessages.get([conversation, msgid]);
      if (dbMessage && dbMessage.version < newVersion) {
        dbMessage.version = newVersion;
        dbMessage.body = newBody;
        await this._db.directMessages.put(dbMessage);
      }
    });
  }

  public async deleteDirectMessage(conversation: string, msgid: string, newVersion: number) {
    await this._db.transaction("rw", this._db.directMessages, async () => {
      const dbMessage = await this._db.directMessages.get([conversation, msgid]);
      if (dbMessage && dbMessage.version < newVersion) {
        dbMessage.version = newVersion;
        dbMessage.body = null;
        await this._db.directMessages.put(dbMessage);
      }
    });
  }

  public async putRoomMessage(msg: RoomMessage) {
    await this._db.transaction("rw", this._db.roomMessages, async () => {
      const dbMessage = await this._db.roomMessages.get([msg.roomname, msg.msgid]);
      if (!dbMessage || dbMessage.version < msg.version) {
        await this._db.roomMessages.put(msg);
      }
    });
  }

  public async editRoomMessage(
    roomname: string,
    msgid: string,
    newVersion: number,
    newBody: string
  ) {
    await this._db.transaction("rw", this._db.roomMessages, async () => {
      const dbMessage = await this._db.roomMessages.get([roomname, msgid]);
      if (dbMessage && dbMessage.version < newVersion) {
        dbMessage.version = newVersion;
        dbMessage.body = newBody;
        await this._db.roomMessages.put(dbMessage);
      }
    });
  }

  public async deleteRoomMessage(roomname: string, msgid: string, newVersion: number) {
    await this._db.transaction("rw", this._db.roomMessages, async () => {
      const dbMessage = await this._db.roomMessages.get([roomname, msgid]);
      if (dbMessage && dbMessage.version < newVersion) {
        dbMessage.version = newVersion;
        dbMessage.body = null;
        await this._db.roomMessages.put(dbMessage);
      }
    });
  }

  public async markAllConversationsAsClosed() {
    await this._db.conversationInfo.toCollection().modify((c) => {
      c.opened = false;
    });
  }

  public async putConversationInfo(info: ConversationInfo) {
    await this._db.conversationInfo.put(info);
  }

  public async updateConversationInfo(conversation: string, changes: Partial<ConversationInfo>) {
    await this._db.conversationInfo.update(conversation, changes);
  }

  public async updateStarredRooms(starredRooms: StarredRoom[]) {
    await this._db.transaction("rw", this._db.starredRooms, async () => {
      await this._db.starredRooms.clear();
      await this._db.starredRooms.bulkAdd(starredRooms);
    });
  }

  public async updateFriends(friends: Friend[]) {
    await this._db.transaction("rw", this._db.friends, async () => {
      await this._db.friends.clear();
      await this._db.friends.bulkAdd(friends);
    });
  }

  public async putFriend(friend: Friend) {
    await this._db.friends.put(friend);
  }

  public async deleteFriend(username: string) {
    await this._db.friends.delete(username);
  }
}
