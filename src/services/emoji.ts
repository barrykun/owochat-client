import Session from "../service/session";

import { writable, Writable } from "svelte/store";

import { endpoints } from "../service/endpoints";

import { TypeOf } from "../util/decoder";

import { serverEventT } from "../service/data";

type Emoji = {
  id: string;
  type: "user" | "room";
  shortcode: string;
  shortcodeLower: string;
  slug: string;
};

export class EmojiService {
  private _session: Session;

  private _myEmojiStore: Writable<Emoji[]>;
  private _roomEmojiStores: Map<string, Writable<Emoji[]>>;

  public constructor(session: Session) {
    this._session = session;
    this._myEmojiStore = writable([]);
    this._roomEmojiStores = new Map();
    this._fetchMyEmoji();
  }

  public async upload(file: File): Promise<string> {
    const res = await fetch("/api/emoji", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
    const json: any = await res.json();
    if (typeof json === "object" && "slug" in json && typeof json.slug === "string") {
      return json.slug;
    } else {
      throw new Error("can't decode response");
    }
  }

  public async uploadToMyStore(shortcode: string, file: File) {
    const slug = await this.upload(file);
    await this.addToMyStore(shortcode, slug);
  }

  public async uploadToRoomStore(roomname: string, shortcode: string, file: File) {
    const slug = await this.upload(file);
    await this.addToRoomStore(roomname, shortcode, slug);
  }

  public async addToMyStore(shortcode: string, slug: string) {
    await this._session.call(endpoints.putUserEmoji, {
      username: this._session.myUsername,
      shortcode,
      slug,
    });
  }

  public async deleteFromMyStore(shortcode: string) {
    await this._session.call(endpoints.deleteUserEmoji, {
      username: this._session.myUsername,
      shortcode,
    });
  }

  public async addToRoomStore(roomname: string, shortcode: string, slug: string) {
    await this._session.call(endpoints.putRoomEmoji, {
      roomname,
      shortcode,
      slug,
    });
  }

  public async deleteFromRoomStore(roomname: string, shortcode: string) {
    await this._session.call(endpoints.deleteRoomEmoji, {
      roomname,
      shortcode,
    });
  }

  public getMyEmojiStore() {
    return this._myEmojiStore;
  }

  public getRoomEmojiStore(roomname: string) {
    let store = this._roomEmojiStores.get(roomname);
    if (!store) {
      store = writable([]);
      this._fetchRoomEmoji(roomname);
      this._roomEmojiStores.set(roomname, store);
    }
    return store;
  }

  private async _fetchRoomEmoji(roomname: string) {
    const roomEmoji = await this._session.call(endpoints.getRoomEmoji, { roomname });
    let store = this._roomEmojiStores.get(roomname);
    if (!store) {
      store = writable([]);
      this._fetchRoomEmoji(roomname);
      this._roomEmojiStores.set(roomname, store);
    }
    store.set(
      roomEmoji.map((e) => ({
        id: "room:" + e.shortcode,
        shortcodeLower: e.shortcode.toLowerCase(),
        type: "room",
        ...e,
      }))
    );
  }

  private async _fetchMyEmoji() {
    const res = await this._session.call(endpoints.getUserEmoji, {
      username: this._session.myUsername,
    });
    this._myEmojiStore.set(
      res.map((e) => ({
        id: "user:" + e.shortcode,
        shortcodeLower: e.shortcode.toLowerCase(),
        type: "user",
        ...e,
      }))
    );
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    let store;
    switch (evt.type) {
      case "user-emoji-add":
        if (evt.username === this._session.myUsername) {
          this._myEmojiStore.update((emoji) => {
            emoji = emoji.filter((e) => e.shortcode != evt.shortcode);
            emoji.push({
              type: "user",
              id: "user:" + evt.shortcode,
              shortcode: evt.shortcode,
              shortcodeLower: evt.shortcode.toLowerCase(),
              slug: evt.slug,
            });
            return emoji;
          });
        }
        break;
      case "user-emoji-remove":
        if (evt.username === this._session.myUsername) {
          this._myEmojiStore.update((emoji) => {
            return emoji.filter((e) => e.shortcode != evt.shortcode);
          });
        }
        break;
      case "room-emoji-add":
        store = this._roomEmojiStores.get(evt.roomname);
        if (!store) {
          store = writable([]);
          this._fetchRoomEmoji(evt.roomname);
          this._roomEmojiStores.set(evt.roomname, store);
        }
        store.update((emoji) => {
          emoji = emoji.filter((e) => e.shortcode != evt.shortcode);
          emoji.push({
            type: "room",
            id: "room:" + evt.shortcode,
            shortcode: evt.shortcode,
            shortcodeLower: evt.shortcode.toLowerCase(),
            slug: evt.slug,
          });
          return emoji;
        });
        break;
      case "room-emoji-remove":
        store = this._roomEmojiStores.get(evt.roomname);
        if (!store) {
          return;
        }
        store.update((emoji) => {
          return emoji.filter((e) => e.shortcode != evt.shortcode);
        });
        break;
    }
  }

  public onStale() {}

  public emojiUrl(shortcode: string): string {
    return "/data/emoji/" + shortcode + ".png";
  }
}
