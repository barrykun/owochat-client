import { Writable, Readable, writable } from "svelte/store";
import { TypeOf } from "../util/decoder";
import { serverEventT } from "../service/data";

export class UpdatesService {
  private _currentCommit: string;
  private _latestCommit: string;
  private _newVersionExistsStore: Writable<boolean>;
  private _lastVersionCheck: Date;

  public constructor(currentCommit: string) {
    this._currentCommit = currentCommit;
    this._latestCommit = currentCommit;
    this._newVersionExistsStore = writable(false);
    this._lastVersionCheck = new Date();
    this.checkVersion();
  }

  public getNewVersionExistsStore(): Readable<boolean> {
    return this._newVersionExistsStore;
  }

  public async checkVersion() {
    const res = await fetch("/app/build/commit-hash");
    if (res.ok) {
      const commitHash = await res.text();
      this._latestCommit = commitHash;
      this._updateStores();
      this._lastVersionCheck = new Date();
    }
  }

  public async infrequentVersionCheck() {
    if (Date.now() - this._lastVersionCheck.getTime() > 1800 * 1000) {
      await this.checkVersion();
    }
  }

  private _updateStores() {
    if (this._currentCommit === this._latestCommit) {
      this._newVersionExistsStore.set(false);
    } else {
      this._newVersionExistsStore.set(true);
    }
  }

  public doUpdate() {
    if (navigator.serviceWorker.controller) {
      navigator.serviceWorker.controller.postMessage({ type: "reload" });
      // in case SW reload fails:
      setTimeout(() => {
        window.location.reload(true);
      }, 5000);
    } else {
      window.location.reload(true);
    }
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    switch (evt.type) {
      case "update-check":
        this._latestCommit = evt.commitHash;
        this._updateStores();
        this._lastVersionCheck = new Date();
        break;
    }
  }
}
