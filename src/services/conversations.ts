import { Writable, Readable, writable } from "svelte/store";

import Session from "../service/session";
import { serverEventT } from "../service/data";
import { TypeOf } from "../util/decoder";
import { DatabaseService, ConversationInfo as DbConversationInfo } from "./database";
import { ConversationInfo } from "../types";
import { endpoints } from "../service/endpoints";

function defaultConversationInfo(partner: string): ConversationInfo {
  return {
    partner,
    lastActivity: new Date(),
    opened: false,
    profileOpen: true,
    lastMsgid: null,
    lastReadMsgid: null,
    partnerLastReadMsgid: null,
  };
}

function fromDbConversationInfo(dbc: DbConversationInfo): ConversationInfo {
  return {
    partner: dbc.conversation,
    lastActivity: dbc.lastActivity,
    opened: dbc.opened,
    profileOpen: dbc.profileOpen,
    lastMsgid: dbc.lastMsgid,
    lastReadMsgid: dbc.lastReadMsgid,
    partnerLastReadMsgid: dbc.partnerLastReadMsgid,
  };
}

function toDbConversationInfo(c: ConversationInfo): DbConversationInfo {
  return {
    conversation: c.partner,
    lastActivity: c.lastActivity,
    opened: c.opened,
    profileOpen: c.profileOpen,
    lastMsgid: c.lastMsgid,
    lastReadMsgid: c.lastReadMsgid,
    partnerLastReadMsgid: c.partnerLastReadMsgid,
  };
}

class Conversation {
  private _info: ConversationInfo;
  private _store: Writable<ConversationInfo>;
  private _authoritative: boolean;

  public constructor(partner: string) {
    this._info = defaultConversationInfo(partner);
    this._store = writable(this._info);
    this._authoritative = false;
  }

  public update(f: (info: ConversationInfo) => void) {
    f(this._info);
    this._store.set(this._info);
  }

  public getInfo(): ConversationInfo {
    return this._info;
  }

  public setInfo(authoritative: boolean, info: ConversationInfo) {
    if (authoritative) {
      this._authoritative = true;
    } else if (this._authoritative) {
      return;
    }
    this._info = info;
    this._store.set(this._info);
  }

  public getStore(): Readable<ConversationInfo> {
    return this._store;
  }
}

interface OpenConversation {
  partner: string;
  lastActivity: Date | null;
}

function compareLastActivity(a: OpenConversation, b: OpenConversation): number {
  const na = (a.lastActivity || 0).valueOf();
  const nb = (b.lastActivity || 0).valueOf();
  return nb - na;
}

class OpenConversations {
  private _list: OpenConversation[];
  private _store: Writable<OpenConversation[]>;

  public constructor() {
    this._list = [];
    this._store = writable(this._list);
  }

  public clear() {
    this._list = [];
    this._store.set(this._list);
  }

  public setFromListUnsortedDestructive(li: OpenConversation[]) {
    li.sort(compareLastActivity);
    this._list = li;
    this._store.set(this._list);
  }

  public update(partner: string, lastActivity: Date) {
    this._list = this._list.filter((oc) => oc.partner !== partner);
    this._list.push({ partner, lastActivity });
    this._list.sort(compareLastActivity);
    this._store.set(this._list);
  }

  public remove(partner: string) {
    this._list = this._list.filter((oc) => oc.partner !== partner);
    this._store.set(this._list);
  }

  public getStore(): Readable<OpenConversation[]> {
    return this._store;
  }
}

export class ConversationsService {
  private _session: Session;

  private _db: DatabaseService;

  private _openConversations: OpenConversations;
  private _conversations: Map<string, Conversation>;

  private _currentPartner: string | null;

  public constructor(db: DatabaseService, session: Session) {
    this._db = db;
    this._session = session;
    this._conversations = new Map();
    this._openConversations = new OpenConversations();
    this._currentPartner = null;
    this.refreshConversations();
  }

  public async setCurrentPartner(partner: string | null) {
    this._currentPartner = partner;
    if (partner) {
      const conversation = this.getConversation(partner);
      const info = conversation.getInfo();
      if (info.lastMsgid && info.lastMsgid !== info.lastReadMsgid) {
        this.markReadUntil(partner, info.lastMsgid);
      }
    }
  }

  public async fetchConversation(partner: string) {
    const rc = await this._session.call(endpoints.getConversation, { partner });
    const c = this.getConversation(partner);
    c.update((info) => {
      info.opened = rc.opened;
      info.lastMsgid = rc.lastMsgid;
      info.lastReadMsgid = rc.srcLastReadMsgid;
      info.partnerLastReadMsgid = rc.dstLastReadMsgid;
      info.lastActivity = rc.lastActivity;
    });
    await this._db.updateConversationInfo(partner, {
      opened: rc.opened,
      lastMsgid: rc.lastMsgid,
      lastReadMsgid: rc.srcLastReadMsgid,
      partnerLastReadMsgid: rc.dstLastReadMsgid,
      lastActivity: rc.lastActivity,
    });
  }

  public async fetchOpenConversations() {
    this._db.markAllConversationsAsClosed();
    const response = await this._session.call(endpoints.getOpenConversations, {});
    const openConversations = [];
    for (const c of response.conversations) {
      // c.src here always refers to us
      this.getConversation(c.dst).update((info) => {
        info.opened = c.opened;
        info.lastMsgid = c.lastMsgid;
        info.lastReadMsgid = c.srcLastReadMsgid;
        info.partnerLastReadMsgid = c.dstLastReadMsgid;
        info.lastActivity = c.lastActivity;
      });
      this._db.updateConversationInfo(c.dst, {
        conversation: c.dst,
        opened: c.opened,
        lastMsgid: c.lastMsgid,
        lastReadMsgid: c.srcLastReadMsgid,
        partnerLastReadMsgid: c.dstLastReadMsgid,
        lastActivity: c.lastActivity,
      });
      openConversations.push({ partner: c.dst, lastActivity: c.lastActivity });
    }
    this._openConversations.setFromListUnsortedDestructive(openConversations);
  }

  public async preloadConversations() {
    // TODO: really only need to preload open ones
    const conversations = await this._db.getAllConversations();
    for (const conversation of conversations) {
      const info = fromDbConversationInfo(conversation);
      const c = this.getConversation(info.partner);
      c.setInfo(false, info);
    }
  }

  public async refreshConversations() {
    await Promise.all([this.fetchOpenConversations(), this.preloadConversations()]);
  }

  public getOpenConversationsStore(): Readable<OpenConversation[]> {
    return this._openConversations.getStore();
  }

  public getConversation(partner: string): Conversation {
    let conversation = this._conversations.get(partner);
    if (!conversation) {
      conversation = new Conversation(partner);
      this._conversations.set(partner, conversation);
      this.fetchConversation(partner);
    }
    return conversation;
  }

  public async setConversationProfileOpen(partner: string, profileOpen: boolean) {
    const c = this.getConversation(partner);
    c.update((info) => {
      info.profileOpen = profileOpen;
    });
    await this._db.updateConversationInfo(partner, {
      profileOpen,
    });
  }

  public async putConversationOpened(partner: string, opened: boolean) {
    await this._session.call(endpoints.putConversationOpened, { partner, opened });
    // TODO: already know this will be accepted, so can pre-emptively do it
  }

  public async markReadUntil(partner: string, lastReadMsgid: string) {
    await this._session.call(endpoints.postConversationReadUntil, { partner, lastReadMsgid });
    // TODO: already know this will be accepted, so can pre-emptively do it
  }

  private _handleDirectMessageNew(
    evt: TypeOf<typeof serverEventT> & { type: "direct-message-new" }
  ) {
    // Figure out who we're talking to
    const partner = evt.src === this._session.myUsername ? evt.dst : evt.src;

    // Update the conversation store
    const c = this.getConversation(partner);
    c.update((info) => {
      info.lastActivity = evt.time;
      info.lastMsgid = evt.msgid;
      info.opened = true;
    });

    // Mark as read if we're looking at this conversation
    if (partner === this._currentPartner) {
      c.update((info) => {
        info.lastReadMsgid = evt.msgid;
      });
      this.markReadUntil(partner, evt.msgid);
    }

    // If we sent this, we also read this (and everything before it)
    if (evt.src === this._session.myUsername) {
      c.update((info) => {
        info.lastReadMsgid = evt.msgid;
      });
    }

    // Put the conversation into the database (TODO: have a close look at this, might need to be an upsert)
    this._db.putConversationInfo(toDbConversationInfo(c.getInfo()));

    // Put this conversation as the first one in the list of open conversations
    this._openConversations.update(partner, evt.time);
  }

  private _handleConversationOpened(
    evt: TypeOf<typeof serverEventT> & { type: "conversation-opened" }
  ) {
    // evt.src is guaranteed to be us
    const c = this.getConversation(evt.dst);
    c.update((info) => {
      info.opened = evt.opened;
    });
    if (evt.opened) {
      this._openConversations.update(evt.dst, evt.time);
    } else {
      this._openConversations.remove(evt.dst);
    }
    this._db.updateConversationInfo(evt.dst, {
      opened: evt.opened,
    });
  }

  private _handleConversationReadUntil(
    evt: TypeOf<typeof serverEventT> & { type: "conversation-read-until" }
  ) {
    if (evt.src === this._session.myUsername) {
      const c = this.getConversation(evt.dst);
      c.update((info) => {
        info.lastReadMsgid = evt.lastReadMsgid;
      });
      this._db.updateConversationInfo(evt.dst, {
        lastReadMsgid: evt.lastReadMsgid,
      });
    } else {
      const c = this.getConversation(evt.src);
      c.update((info) => {
        info.partnerLastReadMsgid = evt.lastReadMsgid;
      });
      this._db.updateConversationInfo(evt.src, {
        partnerLastReadMsgid: evt.lastReadMsgid,
      });
    }
  }

  public handleEvent(evt: TypeOf<typeof serverEventT>) {
    switch (evt.type) {
      case "direct-message-new":
        this._handleDirectMessageNew(evt);
        break;
      case "conversation-opened":
        this._handleConversationOpened(evt);
        break;
      case "conversation-read-until":
        this._handleConversationReadUntil(evt);
        break;
    }
  }
}
