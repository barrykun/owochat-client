import { TypeOf, parseJson, dumpJson } from "../util/decoder";
import { serverFrameT, clientFrameT, serverEventT } from "../service/data";
import { Timeout } from "../util/timeout";
import { Readable, Writable, writable } from "svelte/store";
import Session from "../service/session";

export interface SubscriberConfig {
  onEvent: (event: TypeOf<typeof serverEventT>) => void;
  onListen: (channel: string) => void;
  onUnlisten: (channel: string) => void;
  onStale: () => void;
  onReady: () => void;
  onDisconnect: () => void;
  onDenied: () => void;
}

type SubscriberState = "init" | "connecting" | "connected" | "ready" | "waiting" | "denied";

const PING_TIMEOUT = 30000;
const WEBSOCKET_URL = `${window.location.protocol === "http:" ? "ws" : "wss"}://${
  window.location.host
}/api/ws`;

export class Subscriber {
  private _session: Session;

  private _state: SubscriberState;
  private _stateStore: Writable<SubscriberState>;

  private _config: SubscriberConfig;

  private _ws: WebSocket | null;
  private _streamId: string | null;
  private _lastEventId: number | null;

  private _channelListeners: Map<string, { active: boolean; count: number }>;

  private _reconnects: number;
  private _reconnectTimeout: Timeout;

  private _pingerTimeout: Timeout;

  public constructor(session: Session, config: SubscriberConfig) {
    this._state = "init";
    this._stateStore = writable(this._state);
    this._session = session;
    this._streamId = null;
    this._lastEventId = null;
    this._ws = null;
    this._channelListeners = new Map();
    this._config = config;
    this._reconnects = 0;
    this._reconnectTimeout = new Timeout(() => {
      this._setState("init");
      this.connect();
    });
    this._pingerTimeout = new Timeout(() => {
      this._ping();
    });
  }

  public listener(channel: string): Readable<null> {
    return {
      subscribe: (f) => {
        f(null);
        this.listen(channel);
        return () => {
          setTimeout(() => {
            this.unlisten(channel);
          }, 5000 + 10000 * Math.random());
        };
      },
    };
  }

  public async listen(channel: string) {
    let rec = this._channelListeners.get(channel);
    if (rec) {
      rec.count += 1;
    } else {
      rec = { active: false, count: 1 };
      this._channelListeners.set(channel, rec);
      await this._syncListeners();
    }
  }

  public async unlisten(channel: string) {
    const rec = this._channelListeners.get(channel);
    if (!rec) {
      return;
    }
    rec.count -= 1;
    await this._syncListeners();
  }

  private async _syncListeners() {
    if (!this._inState("ready") && !this._inState("connected")) {
      return;
    }
    if (!this._ws) {
      return;
    }
    const frames: TypeOf<typeof clientFrameT>[] = [];
    for (const [channel, channelMeta] of this._channelListeners) {
      if (channelMeta.count > 0 && !channelMeta.active) {
        channelMeta.active = true;
        frames.push({
          type: "listen",
          channel,
        });
      }
      if (channelMeta.count === 0) {
        if (channelMeta.active) {
          channelMeta.active = false;
          frames.push({
            type: "unlisten",
            channel,
          });
        }
      } else if (channelMeta.count < 0) {
        throw new Error("channel refcount under 0");
      }
    }
    for (const frame of frames) {
      this._sendFrame(frame);
    }
  }

  public async reconnectIfNotConnected() {
    // TODO
    this._reconnects = 0;
    if (!this._ws) {
      this._setState("init");
      this.connect();
    }
  }

  public connect() {
    if (!this._inState("init")) {
      return;
    }
    if (this._ws) {
      this._ws.close();
      this._ws = null;
    }
    this._setState("connecting");
    this._ws = new WebSocket(WEBSOCKET_URL, ["owo"]);
    this._ws.onclose = (_evt) => {
      console.log("[ws] closed");
      this._onDisconnect();
    };
    this._ws.onerror = (evt) => {
      console.log("[ws] error:", evt);
      this._onDisconnect();
    };
    this._ws.onopen = (_evt) => {
      console.log("[ws] open");
      if (!this._ws) {
        throw new Error("what the heckies");
      }
      this._setState("connected");
      this._sendFrame({
        type: "connect",
        auth: this._session.authorization,
        streamId: this._streamId,
        lastEventId: this._lastEventId,
      });
    };
    this._ws.onmessage = (evt) => {
      const data = evt.data;
      const obj = parseJson(serverFrameT, data);
      if (obj.isOk) {
        const frame = obj.value;
        console.log("[ws] received frame:", frame);
        switch (frame.type) {
          case "connected":
            this._onConnected(frame.streamId);
            break;
          case "denied":
            this._onDenied();
            break;
          case "event":
            this._onEvent(frame.id, frame.event);
            break;
          case "ping":
            this._onPing();
            break;
          case "listen":
            this._onListen(frame.channel);
            break;
          case "unlisten":
            this._onUnlisten(frame.channel);
            break;
        }
      } else {
        console.error("received malformed frame!", data, obj.err);
      }
    };
  }

  private async _onConnected(streamId: string) {
    if (streamId !== this._streamId) {
      this._streamId = streamId;
      this._lastEventId = null;
      this._config.onStale();
      for (const [channel, channelMeta] of this._channelListeners) {
        if (channelMeta.active) {
          this._config.onUnlisten(channel);
          channelMeta.active = false;
        }
      }
      await this._syncListeners();
    }
    this._reconnectTimeout.clear();
    this._reconnects = 0;
    this._setState("ready");
    this._config.onReady();
  }

  private _onDenied() {
    this._setState("denied");
    this._config.onDenied();
  }

  private _onListen(channel: string) {
    const channelMeta = this._channelListeners.get(channel);
    if (channelMeta) {
      channelMeta.active = true;
    } else {
      this._channelListeners.set(channel, {
        active: true,
        count: 0,
      });
    }
    this._config.onListen(channel);
  }

  private _onUnlisten(channel: string) {
    const channelMeta = this._channelListeners.get(channel);
    if (channelMeta) {
      channelMeta.active = false;
      if (channelMeta.count <= 0) {
        this._channelListeners.delete(channel);
      }
    }
    this._config.onUnlisten(channel);
  }

  private _onDisconnect() {
    this._ws = null;
    const delay =
      this._reconnects === 0 ? 0 : 1000 * Math.pow(2, Math.min(4, this._reconnects + 1));
    this._reconnects += 1;
    this._reconnectTimeout.schedule(delay);
    this._setState("waiting");
    this._config.onDisconnect();
  }

  private async _onEvent(eventId: number, event: TypeOf<typeof serverEventT>) {
    this._lastEventId = eventId;
    this._config.onEvent(event);
  }

  private async _onPing() {
    await this._ping();
    this._pingerTimeout.schedule(PING_TIMEOUT);
  }

  private async _ping() {
    this._pingerTimeout.schedule(PING_TIMEOUT);
    if (this._ws) {
      this._sendFrame({
        type: "ack",
        lastEventId: this._lastEventId,
      });
    }
  }

  private async _sendFrame(frame: TypeOf<typeof clientFrameT>) {
    console.log("[ws] sent frame:", frame);
    if (this._ws) {
      const data = dumpJson(clientFrameT, frame);
      this._ws.send(data);
    }
  }

  private _inState(state: SubscriberState): boolean {
    return this._state === state;
  }

  private _setState(newState: SubscriberState) {
    this._state = newState;
    this._stateStore.set(this._state);
  }

  public getStateStore(): Readable<SubscriberState> {
    return this._stateStore;
  }
}
