import Session from "../service/session";

export class UploadsService {
  private _session: Session;

  public constructor(session: Session) {
    this._session = session;
  }

  public async upload(file: File): Promise<string> {
    const res = await fetch("/api/upload", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + this._session.authorization,
      },
      body: file,
    });
    const json: any = await res.json();
    if (typeof json === "object" && "slug" in json && typeof json.slug === "string") {
      return json.slug;
    } else {
      throw new Error("couldn't decode upload response");
    }
  }

  public uploadUrl(slug: string): string {
    return "/data/uploads/" + slug;
  }
}
