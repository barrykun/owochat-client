import { Decoder } from "./decoder";
import { Result, Ok, Err } from "./result";

export interface EndpointConfig<Req, Res> {
  method?: string;
  url: (req: Req) => string;
  headers?: (req: Req) => { [header: string]: string };
  encoder?: (req: Req) => string | null;
  query?: (req: Req) => { [key: string]: string | number | boolean | null | undefined };
  decoder?: (res: Response) => Promise<Res>;
}

export type Env = { authorization: string | null };

function queryToString(params: {
  [key: string]: string | number | boolean | null | undefined;
}): string {
  let out = "";
  let first = true;
  for (const key of Object.keys(params)) {
    const value = params[key];
    if (!value) {
      continue;
    }
    if (first) {
      first = false;
    } else {
      out += "&";
    }
    out += `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
  }
  return out;
}

export class Endpoint<Req, Res> {
  private _config: EndpointConfig<Req, Res>;

  public constructor(config: EndpointConfig<Req, Res>) {
    this._config = config;
  }

  public async call(env: Env, req: Req): Promise<Res> {
    const method = this._config.method || "GET";
    let url = this._config.url(req);
    const headers = this._config.headers ? this._config.headers(req) : {};
    if (env.authorization) {
      headers["Authorization"] = "Bearer " + env.authorization;
    }
    const body = this._config.encoder ? this._config.encoder(req) : null;
    if (this._config.query) {
      const query = this._config.query(req);
      const qs = queryToString(query);
      if (qs) {
        url += "?" + qs;
      }
    }
    const res = await fetch(url, {
      method,
      headers,
      body,
    });
    if (this._config.decoder) {
      const dec = await this._config.decoder(res);
      return dec;
    } else {
      return null as never; // this only happens when we're not interested in the result
      // TODO: find out a better way to do this
    }
  }
}

export function throwingJsonDecoder<T>(decoder: Decoder<T>): (res: Response) => Promise<T> {
  const inner = jsonDecoder<T>(decoder);
  return async (res: Response): Promise<T> => {
    const ret = await inner(res);
    if (ret.isOk) {
      return ret.value;
    } else {
      throw new Error(showEndpointError(ret.err));
    }
  };
}

export function jsonDecoder<T>(decoder: Decoder<T>): (res: Response) => Promise<EndpointResult<T>> {
  return async (res: Response): Promise<EndpointResult<T>> => {
    if (res.ok) {
      const ret = decoder.decode(await res.json());
      if (ret.isOk) {
        return Ok(ret.value);
      } else {
        return Err({ type: "json", err: ret.err });
      }
    } else {
      switch (res.status) {
        case 403:
          return Err({ type: "forbidden", reason: "TODO" });
        case 404:
          return Err({ type: "not-found" });
        case 500:
          return Err({ type: "internal-server" });
        default:
          return Err({ type: "other", text: "status code not recognized" });
      }
    }
  };
}

export function showEndpointError(err: EndpointError): string {
  switch (err.type) {
    case "internal-server":
      return "internal server error";
    case "not-found":
      return "not found";
    case "forbidden":
      return "forbidden for reason: " + err.reason;
    case "other":
      return "other error: " + err.text;
    case "json":
      return "json parse error in decoding response: " + err.err;
  }
}

export type EndpointError =
  | { type: "not-found" }
  | { type: "forbidden"; reason: string }
  | { type: "internal-server" }
  | { type: "other"; text: string }
  | { type: "json"; err: string };

export type EndpointResult<T> = Result<T, EndpointError>;
