export type Result<T, E> =
  | { isOk: true; isErr: false; value: T }
  | { isOk: false; isErr: true; err: E };

export function Ok<T, E>(value: T): Result<T, E> {
  return { isOk: true, isErr: false, value };
}

export function Err<T, E>(err: E): Result<T, E> {
  return { isOk: false, isErr: true, err };
}
