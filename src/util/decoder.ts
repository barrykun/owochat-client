import { Result, Ok, Err } from "./result";

export type DecoderResult<T> = Result<T, string>;

export interface Encoder<T> {
  encode: (value: T) => unknown;
}

export interface Decoder<T> {
  decode: (json: unknown) => DecoderResult<T>;
}

export interface Transcoder<T> extends Encoder<T>, Decoder<T> {}

export type TypeOf<T> = T extends Transcoder<infer U> ? U : never;

type Primitive = string | number | symbol;

export const nullT: Transcoder<null> = {
  encode: (str: null) => str,
  decode: (json: unknown) => {
    if (json === null) {
      return Ok(json);
    } else {
      return Err("not null");
    }
  },
};

export const undefinedT: Transcoder<undefined> = {
  encode: (str: undefined) => str,
  decode: (json: unknown) => {
    if (json === undefined) {
      return Ok(json);
    } else {
      return Err("not undefined");
    }
  },
};

export const booleanT: Transcoder<boolean> = {
  encode: (bool: boolean) => bool,
  decode: (json: unknown) => {
    if (typeof json === "boolean") {
      return Ok(json);
    } else {
      return Err("not a boolean");
    }
  },
};

export const numberT: Transcoder<number> = {
  encode: (num: number) => num,
  decode: (json: unknown) => {
    if (typeof json === "number") {
      return Ok(json);
    } else {
      return Err("not a number");
    }
  },
};

export const stringT: Transcoder<string> = {
  encode: (str: string) => str,
  decode: (json: unknown) => {
    if (typeof json === "string") {
      return Ok(json);
    } else {
      return Err("not a string");
    }
  },
};

export const stringDateT: Transcoder<Date> = {
  encode: (date: Date) => date.toISOString(),
  decode: (json: unknown) => {
    if (typeof json === "string") {
      return Ok(new Date(json));
    } else {
      return Err("not a string containing a valid date");
    }
  },
};

export function literalT<T extends Primitive>(literal: T): Transcoder<T> {
  return {
    encode: (lit: T) => lit,
    decode: (json: unknown) => {
      if (json === literal) {
        return Ok(literal);
      } else {
        return Err("not literal");
      }
    },
  };
}

export function nullableT<T>(inner: Transcoder<T>): Transcoder<T | null> {
  return {
    encode: (val: T | null | undefined) => val,
    decode: (json: unknown) => {
      if (json === null || json === undefined) {
        return Ok(null);
      } else {
        return inner.decode(json);
      }
    },
  };
}

export function arrayT<T>(inner: Transcoder<T>): Transcoder<T[]> {
  return {
    encode: (ts: T[]) => ts.map((t) => inner.encode(t)),
    decode: (json: unknown): DecoderResult<T[]> => {
      if (json instanceof Array) {
        const arr: T[] = [];
        for (const j of json) {
          const dec = inner.decode(j);
          if (dec.isOk) {
            arr.push(dec.value);
          } else if (dec.isErr) {
            return Err("in array: " + dec.err);
          }
        }
        return Ok(arr);
      } else {
        return Err("not an array");
      }
    },
  };
}

export type ObjectTranscoder<T> = { [K in keyof T]: Transcoder<T[K]> };
export function objectT<Keys>(inner: ObjectTranscoder<Keys>): Transcoder<Keys> {
  return {
    encode: (obj: Keys) => {
      const ret = {} as any;
      for (const key of Object.keys(inner)) {
        const k = key as keyof Keys;
        const transcoder = inner[k];
        const val = obj[k];
        const jval = transcoder.encode(val);
        ret[k] = jval;
      }
      return ret;
    },
    decode: (json: unknown) => {
      if (json && typeof json === "object") {
        const ret = {} as any;
        for (const key of Object.keys(inner)) {
          const k = key as keyof Keys;
          const transcoder = inner[k];
          const jval = (json as any)[k];
          const val = transcoder.decode(jval);
          if (val.isOk) {
            ret[k] = val.value;
          } else if (val.isErr) {
            return Err("in object, key " + k + ": " + val.err);
          }
        }
        return Ok(ret);
      } else {
        return Err("not an object");
      }
    },
  };
}

export type PluckTranscoder<New> = { [K in keyof New]: [string, Transcoder<New[K]>] };
export function pluckT<New>(inner: PluckTranscoder<New>): Transcoder<New> {
  return {
    encode: (obj: New) => {
      const ret = {} as any;
      for (const key of Object.keys(inner)) {
        const k = key as keyof New;
        const rec = inner[k];
        const val = obj[k];
        const jval = rec[1].encode(val);
        ret[rec[0]] = jval;
      }
      return ret;
    },
    decode: (json: unknown) => {
      if (json && typeof json === "object") {
        const ret = {} as any;
        for (const key of Object.keys(inner)) {
          const k = key as keyof New;
          const rec = inner[k];
          const jval = (json as any)[rec[0]];
          const val = rec[1].decode(jval);
          if (val.isOk) {
            ret[k] = val.value;
          } else if (val.isErr) {
            return Err("in object, key " + k + ": " + val.err);
          }
        }
        return Ok(ret);
      } else {
        return Err("not an object");
      }
    },
  };
}

type DiscriminatedUnion<D extends Primitive> = { [key: string]: Record<D, string> };
type SumTranscoder<D extends Primitive, Keys extends DiscriminatedUnion<D>> = {
  [K in keyof Keys]: Transcoder<Keys[K]>;
};
export function sumT<D extends Primitive, Keys extends DiscriminatedUnion<D>>(
  discriminant: D,
  inner: SumTranscoder<D, Keys>
): Transcoder<Keys[keyof Keys]> {
  return {
    encode: (val: Keys[keyof Keys]) => {
      const disc = val[discriminant];
      const transcoder = inner[disc];
      // TODO: look into typechecking
      return (transcoder as any).encode(val);
    },
    decode: (json: unknown) => {
      if (json && typeof json === "object") {
        if (discriminant in json) {
          const disc = (json as any)[discriminant];
          if (disc in inner) {
            const transcoder = inner[disc];
            return transcoder.decode(json);
          } else {
            return Err("sum type discriminant " + disc + " not valid");
          }
        } else {
          return Err("discriminant key " + discriminant + " not found");
        }
      } else {
        return Err("not an object");
      }
    },
  };
}

export function jsonStringT<New>(inner: Transcoder<New>): Transcoder<New> {
  return {
    encode: (val: New) => {
      return dumpJson(inner, val);
    },
    decode: (json: unknown) => {
      if (typeof json === "string") {
        return parseJson(inner, json);
      } else {
        return Err("not a string");
      }
    },
  };
}

export function stringEnumT<P extends string>(...variants: P[]): Transcoder<P> {
  return {
    encode: (val: P) => {
      return val;
    },
    decode: (json: unknown) => {
      if (typeof json === "string") {
        for (const variant of variants) {
          if (json === variant) {
            return Ok(variant);
          }
        }
        return Err("not a valid variant, expecting one of " + variants.join(", "));
      } else {
        return Err("not a string");
      }
    },
  };
}

export function parseJson<T>(decoder: Decoder<T>, text: string): DecoderResult<T> {
  let obj;
  try {
    obj = JSON.parse(text);
  } catch (_e) {
    return Err("json parse failure");
  }
  return decoder.decode(obj);
}

export function dumpJson<T>(encoder: Encoder<T>, item: T): string {
  const obj = encoder.encode(item);
  return JSON.stringify(obj);
}
