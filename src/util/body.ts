const EMOJI_ARG_REGEX = /^[0-9A-Za-z-_]+,[0-9A-Za-z-_]+$/;
const UPLOAD_SLUG_REGEX = /^[0-9A-Za-z]+\.[a-z]+$/;
const BUTTON_ARG_REGEX = /^([^:]+):(.+)$/;
const URL_REGEX = /^https?:\/\/[^ ]+$/;
const PADDING_ARG_REGEX = /^([0-9]+)px$/;
const BORDER_ARG_REGEX = /^(#[0-9a-fA-F]{3}|#[0-9a-fA-F]{6}),([0-9]+)px$/;
const YOUTUBE_VIDEOID_REGEX = /^[a-zA-Z0-9_-]+$/;

type BoxOrientation = "horizontal" | "vertical";

export type BoxChild = { layout: string; nodes: Node[] };

type Alignment = "left" | "center" | "right";

export type Node =
  | {
      type: "n";
      children: Node[];
    }
  | {
      type: "box";
      orientation: BoxOrientation;
      children: BoxChild[];
    }
  | {
      type: "spoiler";
      label: string;
      children: Node[];
    }
  | {
      type: "text";
      style: Style;
      text: string;
    }
  | {
      type: "mention";
      username: string;
    }
  | {
      type: "room";
      roomname: string;
    }
  | {
      type: "emoji";
      shortcode: string;
      slug: string;
    }
  | {
      type: "image";
      slug: string;
    }
  | {
      type: "link";
      href: string;
    }
  | {
      type: "button";
      action: string;
      children: Node[];
    }
  | {
      type: "border";
      width: number;
      color: string;
      children: Node[];
    }
  | {
      type: "padding";
      size: number;
      children: Node[];
    }
  | {
      type: "align";
      align: Alignment;
      children: Node[];
    }
  | {
      type: "youtube";
      videoId: string;
    };

export type Style = {
  bold: boolean | null;
  italic: boolean | null;
  strikeout: boolean | null;
  underline: boolean | null;
  color: string | null;
  font: string | null;
  size: number | null;
  outlineColor: string | null;
};

type Tag =
  | { type: "b"; ending: boolean }
  | { type: "i"; ending: boolean }
  | { type: "s"; ending: boolean }
  | { type: "u"; ending: boolean }
  | { type: "fc"; ending: false; color: string }
  | { type: "fc"; ending: true }
  | { type: "ff"; ending: false; font: string }
  | { type: "ff"; ending: true }
  | { type: "fs"; ending: false; size: number }
  | { type: "fs"; ending: true }
  | { type: "fo"; ending: false; color: string }
  | { type: "fo"; ending: true }
  | { type: "m"; username: string }
  | { type: "room"; roomname: string }
  | { type: "img"; slug: string }
  | { type: "url"; href: string }
  | { type: "em"; shortcode: string; slug: string }
  | { type: "button"; ending: false; action: string }
  | { type: "button"; ending: true }
  | { type: "n"; ending: boolean }
  | { type: "box"; ending: false; orientation: BoxOrientation; layout: string[] }
  | { type: "box"; ending: true; orientation: BoxOrientation }
  | { type: "spoiler"; ending: false; label: string }
  | { type: "spoiler"; ending: true }
  | { type: "border"; ending: false; width: number; color: string }
  | { type: "border"; ending: true }
  | { type: "padding"; ending: false; size: number }
  | { type: "padding"; ending: true }
  | { type: "align"; ending: false; align: Alignment }
  | { type: "align"; ending: true }
  | { type: "youtube"; videoId: string };

type Tok = { type: "text"; text: string } | { type: "tag"; tag: Tag };

type TokState = "init" | "maybe-tag" | "tag";

function processTag(config: BbCodeConfig, text: string): Tag | null {
  let inner;
  if (text.length >= 3 && text[0] === "[" && text[text.length - 1] === "]") {
    inner = text.substring(1, text.length - 1);
  } else {
    return null;
  }
  if (config.b && inner === "b") {
    return {
      type: "b",
      ending: false,
    };
  } else if (config.i && inner === "i") {
    return {
      type: "i",
      ending: false,
    };
  } else if (config.s && inner === "s") {
    return {
      type: "s",
      ending: false,
    };
  } else if (config.u && inner === "u") {
    return {
      type: "u",
      ending: false,
    };
  } else if (config.b && inner === "/b") {
    return {
      type: "b",
      ending: true,
    };
  } else if (config.i && inner === "/i") {
    return {
      type: "i",
      ending: true,
    };
  } else if (config.s && inner === "/s") {
    return {
      type: "s",
      ending: true,
    };
  } else if (config.u && inner === "/u") {
    return {
      type: "u",
      ending: true,
    };
  } else if (config.fc && inner === "/fc") {
    return {
      type: "fc",
      ending: true,
    };
  } else if (config.ff && inner === "/ff") {
    return {
      type: "ff",
      ending: true,
    };
  } else if (config.fs && inner === "/fs") {
    return {
      type: "fs",
      ending: true,
    };
  } else if (config.fs && inner === "/fo") {
    return {
      type: "fo",
      ending: true,
    };
  } else if (config.button && inner === "/button") {
    return {
      type: "button",
      ending: true,
    };
  } else if (config.n && inner === "n") {
    return {
      type: "n",
      ending: false,
    };
  } else if (config.n && inner === "/n") {
    return {
      type: "n",
      ending: true,
    };
  } else if (config.box && (inner === "/hbox" || inner === "/vbox")) {
    const orientation = inner === "/hbox" ? "horizontal" : "vertical";
    return {
      type: "box",
      ending: true,
      orientation,
    };
  } else if (config.spoiler && inner === "spoiler") {
    return {
      type: "spoiler",
      label: "",
      ending: false,
    };
  } else if (config.spoiler && inner === "/spoiler") {
    return {
      type: "spoiler",
      ending: true,
    };
  } else if (config.border && inner === "/border") {
    return {
      type: "border",
      ending: true,
    };
  } else if (config.padding && inner === "/padding") {
    return {
      type: "padding",
      ending: true,
    };
  } else if (config.align && inner === "/align") {
    return {
      type: "align",
      ending: true,
    };
  } else {
    const eqIdx = inner.indexOf("=");
    if (eqIdx === -1) {
      return null;
    }
    const type = inner.substring(0, eqIdx);
    const arg = inner.substring(eqIdx + 1);
    if (
      config.fc &&
      type === "fc" &&
      (/^#[0-9a-fA-F]{3}$/.test(arg) || /^#[0-9a-fA-F]{6}$/.test(arg))
    ) {
      return {
        type: "fc",
        ending: false,
        color: arg,
      };
    } else if (config.ff && type === "ff") {
      // TODO: validate arg (is that even possible here?)
      return {
        type: "ff",
        ending: false,
        font: arg,
      };
    } else if (config.fs && type === "fs") {
      let size = null;
      try {
        size = parseInt(arg, 10);
      } catch (_e) {
        return null;
      }
      if (isNaN(size)) {
        return null;
      }
      if (size < 1) {
        size = 1;
      }
      if (size > 40) {
        size = 40;
      }
      return {
        type: "fs",
        ending: false,
        size,
      };
    } else if (config.m && type === "m" && /^[a-z0-9_-]+$/.test(arg)) {
      return {
        type: "m",
        username: arg,
      };
    } else if (config.room && type === "room" && /^[a-z0-9_-]+$/.test(arg)) {
      return {
        type: "room",
        roomname: arg,
      };
    } else if (config.em && type === "em" && EMOJI_ARG_REGEX.test(arg)) {
      const commaIdx = arg.indexOf(","); // guaranteed to be there and unique by the regex
      const shortcode = arg.substring(0, commaIdx);
      const slug = arg.substring(commaIdx + 1);
      return {
        type: "em",
        shortcode,
        slug,
      };
    } else if (config.img && type === "img" && UPLOAD_SLUG_REGEX.test(arg)) {
      return {
        type: "img",
        slug: arg,
      };
    } else if (config.url && type === "url" && URL_REGEX.test(arg)) {
      return {
        type: "url",
        href: arg,
      };
    } else if (
      config.fo &&
      type === "fo" &&
      (/^#[0-9a-fA-F]{3}$/.test(arg) || /^#[0-9a-fA-F]{6}$/.test(arg))
    ) {
      return {
        type: "fo",
        ending: false,
        color: arg,
      };
    } else if (config.button && type === "button" && BUTTON_ARG_REGEX.test(arg)) {
      return {
        type: "button",
        ending: false,
        action: arg,
      };
    } else if (config.box && (type === "hbox" || type === "vbox")) {
      const layouts = arg.split(",");
      const orientation = type === "hbox" ? "horizontal" : "vertical";
      return {
        type: "box",
        ending: false,
        orientation,
        layout: layouts,
      };
    } else if (config.spoiler && type === "spoiler") {
      return {
        type: "spoiler",
        label: arg,
        ending: false,
      };
    } else if (config.padding && type === "padding" && PADDING_ARG_REGEX.test(arg)) {
      const m = arg.match(PADDING_ARG_REGEX) as RegExpMatchArray; // TODO: bit wasteful to do it twice
      return {
        type: "padding",
        ending: false,
        size: parseInt(m[1], 10),
      };
    } else if (config.border && type === "border" && BORDER_ARG_REGEX.test(arg)) {
      const m = arg.match(BORDER_ARG_REGEX) as RegExpMatchArray; // TODO: bit wasteful to do it twice
      return {
        type: "border",
        ending: false,
        color: m[1],
        width: parseInt(m[2], 10),
      };
    } else if (
      config.align &&
      type === "align" &&
      (arg === "left" || arg === "center" || arg === "right")
    ) {
      return {
        type: "align",
        ending: false,
        align: arg,
      };
    } else if (config.youtube && type === "youtube" && YOUTUBE_VIDEOID_REGEX.test(arg)) {
      return {
        type: "youtube",
        videoId: arg,
      };
    } else {
      return null;
    }
  }
}

function lex(config: BbCodeConfig, text: string): Tok[] {
  let index = 0;
  let buf = "";
  let state: TokState = "init";
  const out: Tok[] = [];
  while (index < text.length) {
    const c = text[index];
    index += 1;
    if (state === "init") {
      if (c === "[") {
        state = "maybe-tag";
      } else {
        buf += c;
      }
    } else if (state === "maybe-tag") {
      if (c === "[") {
        buf += "[[";
        state = "init";
      } else if (c === "]") {
        buf += "[]";
        state = "init";
      } else {
        if (buf !== "") {
          out.push({
            type: "text",
            text: buf,
          });
          buf = "";
        }
        buf += "[";
        buf += c;
        state = "tag";
      }
    } else if (state === "tag") {
      if (c === "]") {
        buf += c;
        const tag = processTag(config, buf);
        if (tag) {
          out.push({
            type: "tag",
            tag,
          });
          buf = "";
        }
        state = "init";
      } else {
        buf += c;
      }
    }
  }
  if (state === "init") {
    if (buf !== "") {
      out.push({
        type: "text",
        text: buf,
      });
    }
  } else if (state === "tag") {
    out.push({
      type: "text",
      text: buf,
    });
  }
  return out;
}

interface TokenProducer {
  next(): Tok | null;
}

class TokenStream {
  private _idx: number;
  private _toks: Tok[];

  public constructor(toks: Tok[]) {
    this._idx = 0;
    this._toks = toks;
  }

  public next(): Tok | null {
    if (this._idx < this._toks.length) {
      const idx = this._idx;
      this._idx += 1;
      return this._toks[idx];
    } else {
      return null;
    }
  }
}

class UntilTokenStream {
  private _inner: TokenProducer;
  private _predicate: (tok: Tok) => boolean;

  public constructor(ts: TokenProducer, predicate: (tok: Tok) => boolean) {
    this._inner = ts;
    this._predicate = predicate;
  }

  public next(): Tok | null {
    const tok = this._inner.next();
    if (!tok) {
      return null;
    } else if (this._predicate(tok)) {
      return null;
    } else {
      return tok;
    }
  }
}

function initialStyle(): Style {
  return {
    bold: null,
    italic: null,
    strikeout: null,
    underline: null,
    color: null,
    outlineColor: null,
    font: null,
    size: null,
  };
}

function cloneStyle(style: Style): Style {
  return {
    bold: style.bold,
    italic: style.italic,
    strikeout: style.strikeout,
    underline: style.underline,
    color: style.color,
    outlineColor: style.outlineColor,
    font: style.font,
    size: style.size,
  };
}

function parse(config: BbCodeConfig, style: Style, ts: TokenProducer): Node[] {
  const nodes: Node[] = [];
  const colorStack: Array<string | null> = [];
  const fontStack: Array<string | null> = [];
  const sizeStack: Array<number | null> = [];
  const outlineColorStack: Array<string | null> = [];

  // eslint-disable-next-line no-constant-condition
  while (true) {
    const tok = ts.next();
    if (!tok) {
      break;
    }
    if (tok.type === "tag") {
      const tag = tok.tag;
      if (tag.type === "b") {
        style.bold = !tag.ending;
      } else if (tag.type === "i") {
        style.italic = !tag.ending;
      } else if (tag.type === "s") {
        style.strikeout = !tag.ending;
      } else if (tag.type === "u") {
        style.underline = !tag.ending;
      } else if (tag.type === "fc") {
        if (tag.ending) {
          style.color = colorStack.pop() || null;
        } else {
          colorStack.push(style.color);
          style.color = tag.color;
        }
      } else if (tag.type === "ff") {
        if (tag.ending) {
          style.font = fontStack.pop() || null;
        } else {
          fontStack.push(style.font);
          style.font = tag.font;
        }
      } else if (tag.type === "fs") {
        if (tag.ending) {
          style.size = sizeStack.pop() || null;
        } else {
          sizeStack.push(style.size);
          style.size = tag.size;
        }
      } else if (tag.type === "fo") {
        if (tag.ending) {
          style.outlineColor = outlineColorStack.pop() || null;
        } else {
          outlineColorStack.push(style.outlineColor);
          style.outlineColor = tag.color;
        }
      } else if (tag.type === "m") {
        nodes.push({
          type: "mention",
          username: tag.username,
        });
      } else if (tag.type === "room") {
        nodes.push({
          type: "room",
          roomname: tag.roomname,
        });
      } else if (tag.type === "em") {
        nodes.push({
          type: "emoji",
          shortcode: tag.shortcode,
          slug: tag.slug,
        });
      } else if (tag.type === "img") {
        nodes.push({
          type: "image",
          slug: tag.slug,
        });
      } else if (tag.type === "url") {
        nodes.push({
          type: "link",
          href: tag.href,
        });
      } else if (tag.type === "youtube") {
        nodes.push({
          type: "youtube",
          videoId: tag.videoId,
        });
      } else if (tag.type === "button" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "button" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "button",
          action: tag.action,
          children,
        });
      } else if (tag.type === "n" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "n" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "n",
          children,
        });
      } else if (tag.type === "box" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) =>
            t.type === "tag" &&
            t.tag.type === "box" &&
            t.tag.ending &&
            t.tag.orientation === tag.orientation
        );
        const innerChildren = parse(config, style, inner);
        const children = [];
        for (let i = 0; i < innerChildren.length && i < tag.layout.length; i++) {
          const child = innerChildren[i];
          const layout = tag.layout[i];
          children.push({
            nodes: [child],
            layout,
          });
        }
        nodes.push({
          type: "box",
          orientation: tag.orientation,
          children,
        });
      } else if (tag.type === "spoiler" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "spoiler" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "spoiler",
          label: tag.label,
          children,
        });
      } else if (tag.type === "border" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "border" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "border",
          width: tag.width,
          color: tag.color,
          children,
        });
      } else if (tag.type === "padding" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "padding" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "padding",
          size: tag.size,
          children,
        });
      } else if (tag.type === "align" && !tag.ending) {
        const inner = new UntilTokenStream(
          ts,
          (t) => t.type === "tag" && t.tag.type === "align" && t.tag.ending
        );
        const children = parse(config, style, inner);
        nodes.push({
          type: "align",
          align: tag.align,
          children,
        });
      }
    } else if (tok.type === "text") {
      nodes.push({
        type: "text",
        style: cloneStyle(style),
        text: tok.text,
      });
    }
  }
  return nodes;
}

export type BbCodeConfig = {
  b: boolean;
  i: boolean;
  s: boolean;
  u: boolean;
  fc: boolean;
  ff: boolean;
  fs: boolean;
  fo: boolean;
  m: boolean;
  room: boolean;
  em: boolean;
  img: boolean;
  url: boolean;
  button: boolean;
  box: boolean;
  spoiler: boolean;
  padding: boolean;
  border: boolean;
  n: boolean;
  align: boolean;
  youtube: boolean;
};

export const fullBbCodeConfig: BbCodeConfig = {
  b: true,
  i: true,
  s: true,
  u: true,
  fc: true,
  ff: true,
  fs: true,
  fo: true,
  m: true,
  room: true,
  em: true,
  img: true,
  url: true,
  button: true,
  box: true,
  spoiler: true,
  padding: true,
  border: true,
  n: true,
  align: true,
  youtube: true,
};

export const minimalBbCodeConfig: BbCodeConfig = {
  b: true,
  i: true,
  s: true,
  u: true,
  fc: true,
  ff: true,
  fs: true,
  fo: true,
  m: false,
  room: false,
  em: true,
  img: false,
  url: false,
  button: false,
  box: false,
  spoiler: false,
  padding: false,
  border: false,
  n: false,
  align: true,
  youtube: false,
};

export function parseBbCode(text: string, config?: BbCodeConfig): Node[] {
  config = config || fullBbCodeConfig;
  const toks = lex(config, text);
  const style = initialStyle();
  const ts = new TokenStream(toks);
  return parse(config, style, ts);
}

export function nodesToPlain(nodes: Node[]): string {
  let text = "";
  for (const node of nodes) {
    text += nodeToPlain(node);
  }
  return text;
}

export function nodeToPlain(node: Node): string {
  switch (node.type) {
    case "text":
      return node.text;
    case "mention":
      return "@" + node.username;
    case "link":
      return node.href;
    case "room":
      return "#" + node.roomname;
    case "emoji":
      return ":" + node.shortcode + ":";
    case "image":
      return "[img]";
    case "button":
      return nodesToPlain(node.children);
    case "spoiler":
      return "[spoiler]";
    case "box":
      return "[box]";
    case "align":
      return nodesToPlain(node.children);
    case "youtube":
      return "https://youtube.com/watch?v=" + node.videoId;
    default:
      return "";
  }
}
