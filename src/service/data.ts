import {
  sumT,
  pluckT,
  nullableT,
  stringT,
  stringDateT,
  numberT,
  booleanT,
  arrayT,
  literalT,
  jsonStringT,
  stringEnumT,
} from "../util/decoder";

export const repeatStringT = stringEnumT("no-repeat", "repeat-x", "repeat-y", "repeat-xy");

export const alignStringT = stringEnumT(
  "top-left",
  "top-right",
  "bottom-left",
  "bottom-right",
  "center"
);

export const backgroundT = pluckT({
  enabled: ["enabled", booleanT],
  image: ["image", booleanT],
  alpha: ["alpha", numberT],
  color: ["color", nullableT(stringT)],
  align: ["align", alignStringT],
  size: ["size", stringT],
  repeat: ["repeat", repeatStringT],
});

export const userInfoExtraFieldT = pluckT({
  field: ["field", stringT],
  value: ["value", stringT],
});

export const userInfoT = pluckT({
  display: ["display", stringT],

  age: ["age", nullableT(stringT)],
  gender: ["gender", nullableT(stringT)],
  location: ["location", nullableT(stringT)],

  bio: ["bio", nullableT(stringT)],

  extraFields: ["extra-fields", arrayT(userInfoExtraFieldT)],

  nameColor: ["name-color", nullableT(stringT)],
  textColor: ["text-color", nullableT(stringT)],

  bannerColor: ["banner-color", nullableT(stringT)],
  buttonColor: ["button-color", nullableT(stringT)],

  messageBackground: ["message-background", backgroundT],
  profileBackground: ["profile-background", backgroundT],

  avatarSetAt: ["avatar-set-at", nullableT(stringDateT)],

  messageBackgroundSetAt: ["message-background-set-at", nullableT(stringDateT)],
  profileBackgroundSetAt: ["profile-background-set-at", nullableT(stringDateT)],

  updatedAt: ["updated-at", stringDateT],
});

export const putUserInfoT = pluckT({
  display: ["display", stringT],

  age: ["age", nullableT(stringT)],
  gender: ["gender", nullableT(stringT)],
  location: ["location", nullableT(stringT)],

  bio: ["bio", nullableT(stringT)],

  extraFields: ["extra-fields", arrayT(userInfoExtraFieldT)],

  nameColor: ["name-color", nullableT(stringT)],
  textColor: ["text-color", nullableT(stringT)],

  bannerColor: ["banner-color", nullableT(stringT)],
  buttonColor: ["button-color", nullableT(stringT)],

  messageBackground: ["message-background", backgroundT],
  profileBackground: ["profile-background", backgroundT],
});

export const roomInfoT = pluckT({
  display: ["display", stringT],
  bio: ["bio", nullableT(stringT)],
  nameColor: ["name-color", nullableT(stringT)],
  backgroundColor: ["background-color", nullableT(stringT)],
  updatedAt: ["updated-at", stringDateT],
});

export const putRoomInfoT = pluckT({
  display: ["display", stringT],
  bio: ["bio", nullableT(stringT)],
  nameColor: ["name-color", nullableT(stringT)],
  backgroundColor: ["background-color", nullableT(stringT)],
});

export const emojiT = pluckT({
  shortcode: ["shortcode", stringT],
  slug: ["slug", stringT],
});

export const serverConfigT = pluckT({
  openRegistration: ["open-registration", booleanT],
  title: ["title", stringT],
  vapidPubkey: ["vapid-pubkey", stringT],
});

export const notificationT = sumT("type", {
  mention: pluckT({
    type: ["type", literalT("mention")],
    notifid: ["notifid", stringT],
    dst: ["dst", stringT],
    createdAt: ["created-at", stringDateT],
    username: ["username", stringT],
    roomname: ["roomname", stringT],
    body: ["body", stringT],
  }),
  "room-message": pluckT({
    type: ["type", literalT("room-message")],
    notifid: ["notifid", stringT],
    dst: ["dst", stringT],
    createdAt: ["created-at", stringDateT],
    username: ["username", stringT],
    roomname: ["roomname", stringT],
    body: ["body", stringT],
  }),
  "direct-message": pluckT({
    type: ["type", literalT("direct-message")],
    notifid: ["notifid", stringT],
    dst: ["dst", stringT],
    createdAt: ["created-at", stringDateT],
    username: ["username", stringT],
    enc: ["enc", stringT],
    body: ["body", stringT],
  }),
});

export const serverEventT = sumT("type", {
  "room-message-new": pluckT({
    type: ["type", literalT("room-message-new")],
    roomname: ["roomname", stringT],
    author: ["author", stringT],
    msgid: ["msgid", stringT],
    body: ["body", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "room-message-edit": pluckT({
    type: ["type", literalT("room-message-edit")],
    roomname: ["roomname", stringT],
    msgid: ["msgid", stringT],
    body: ["body", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "room-message-delete": pluckT({
    type: ["type", literalT("room-message-delete")],
    roomname: ["roomname", stringT],
    msgid: ["msgid", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "direct-message-new": pluckT({
    type: ["type", literalT("direct-message-new")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    msgid: ["msgid", stringT],
    enc: ["enc", stringT],
    body: ["body", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "direct-message-edit": pluckT({
    type: ["type", literalT("direct-message-edit")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    msgid: ["msgid", stringT],
    enc: ["enc", stringT],
    body: ["body", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "direct-message-delete": pluckT({
    type: ["type", literalT("direct-message-delete")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    msgid: ["msgid", stringT],
    version: ["version", numberT],
    time: ["time", stringDateT],
  }),
  "conversation-opened": pluckT({
    type: ["type", literalT("conversation-opened")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    opened: ["opened", booleanT],
    time: ["time", stringDateT],
  }),
  "conversation-read-until": pluckT({
    type: ["type", literalT("conversation-read-until")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    lastReadMsgid: ["last-read-msgid", stringT],
    time: ["time", stringDateT],
  }),
  "room-info-update": pluckT({
    type: ["type", literalT("room-info-update")],
    roomname: ["roomname", stringT],
    time: ["time", stringDateT],
  }),
  "user-info-update": pluckT({
    type: ["type", literalT("user-info-update")],
    username: ["username", stringT],
    time: ["time", stringDateT],
  }),
  "user-starred-add": pluckT({
    type: ["type", literalT("user-starred-add")],
    username: ["username", stringT],
    roomname: ["roomname", stringT],
    notifyMode: ["notify-mode", stringT],
    time: ["time", stringDateT],
  }),
  "user-starred-remove": pluckT({
    type: ["type", literalT("user-starred-remove")],
    username: ["username", stringT],
    roomname: ["roomname", stringT],
    time: ["time", stringDateT],
  }),
  "room-avatar-update": pluckT({
    type: ["type", literalT("room-avatar-update")],
    roomname: ["roomname", stringT],
    time: ["time", stringDateT],
  }),
  "room-emoji-add": pluckT({
    type: ["type", literalT("room-emoji-add")],
    roomname: ["roomname", stringT],
    shortcode: ["shortcode", stringT],
    slug: ["slug", stringT],
    time: ["time", stringDateT],
  }),
  "room-emoji-remove": pluckT({
    type: ["type", literalT("room-emoji-remove")],
    roomname: ["roomname", stringT],
    shortcode: ["shortcode", stringT],
    time: ["time", stringDateT],
  }),
  "user-emoji-add": pluckT({
    type: ["type", literalT("user-emoji-add")],
    username: ["username", stringT],
    shortcode: ["shortcode", stringT],
    slug: ["slug", stringT],
    time: ["time", stringDateT],
  }),
  "user-emoji-remove": pluckT({
    type: ["type", literalT("user-emoji-remove")],
    username: ["username", stringT],
    shortcode: ["shortcode", stringT],
    time: ["time", stringDateT],
  }),
  "user-friend-add": pluckT({
    type: ["type", literalT("user-friend-add")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    time: ["time", stringDateT],
  }),
  "user-friend-remove": pluckT({
    type: ["type", literalT("user-friend-remove")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    time: ["time", stringDateT],
  }),
  "user-notification-add": pluckT({
    type: ["type", literalT("user-notification-add")],
    notification: ["notification", notificationT],
    time: ["time", stringDateT],
  }),
  "user-device-update": pluckT({
    type: ["type", literalT("user-device-update")],
    deviceNumber: ["device-number", numberT],
    time: ["time", stringDateT],
  }),
  "user-device-remove": pluckT({
    type: ["type", literalT("user-device-remove")],
    deviceNumber: ["device-number", numberT],
    time: ["time", stringDateT],
  }),
  "room-role-update": pluckT({
    type: ["type", literalT("room-role-update")],
    roomname: ["roomname", stringT],
    username: ["username", stringT],
    role: ["role", stringT],
    time: ["time", stringDateT],
  }),
  tick: pluckT({
    type: ["type", literalT("tick")],
    src: ["src", stringT],
    dst: ["dst", stringT],
    time: ["time", stringDateT],
  }),
  "update-check": pluckT({
    type: ["type", literalT("update-check")],
    commitHash: ["commit-hash", stringT],
    time: ["time", stringDateT],
  }),
});

export const messageT = pluckT({
  msgid: ["msgid", stringT],
  author: ["author", stringT],
  sent: ["sent", stringDateT],
  body: ["body", nullableT(stringT)],
  version: ["version", numberT],
});

export const directMessageT = pluckT({
  msgid: ["msgid", stringT],
  src: ["src", stringT],
  dst: ["dst", stringT],
  sent: ["sent", stringDateT],
  enc: ["enc", stringT],
  body: ["body", nullableT(stringT)],
  version: ["version", numberT],
});

export const userDeviceMetaT = pluckT({
  token: ["token", stringT],
  name: ["name", stringT],
});

export const roomUserRoleT = pluckT({
  username: ["username", stringT],
  role: ["role", stringT],
});

export const roomSearchRecordT = pluckT({
  roomname: ["roomname", stringT],
  membersOnly: ["members-only", booleanT],
});

export const userSearchRecordT = pluckT({
  username: ["username", stringT],
});

export const sessionInfoT = pluckT({
  username: ["username", stringT],
  token: ["token", stringT],
  isAdmin: ["is-admin", booleanT],
  permissions: [
    "permissions",
    pluckT({
      canInvite: ["can-invite", booleanT],
    }),
  ],
});

export const userPermissionsT = pluckT({
  isAdmin: ["is-admin", booleanT],
  canInvite: ["can-invite", booleanT],
});

export const webpushKeysT = pluckT({
  auth: ["auth", stringT],
  p256dh: ["p256dh", stringT],
});

export const webpushDataT = pluckT({
  endpoint: ["endpoint", stringT],
  keys: ["keys", webpushKeysT],
});

export const conversationT = pluckT({
  src: ["src", stringT],
  dst: ["dst", stringT],
  opened: ["opened", booleanT],
  lastMsgid: ["last-msgid", nullableT(stringT)],
  srcLastReadMsgid: ["src-last-read-msgid", nullableT(stringT)],
  dstLastReadMsgid: ["dst-last-read-msgid", nullableT(stringT)],
  lastActivity: ["last-activity", nullableT(stringDateT)],
});

export const serverFrameT = sumT("type", {
  event: pluckT({
    type: ["type", literalT("event")],
    id: ["id", numberT],
    event: ["data", jsonStringT(serverEventT)],
  }),
  denied: pluckT({
    type: ["type", literalT("denied")],
  }),
  connected: pluckT({
    type: ["type", literalT("connected")],
    streamId: ["stream-id", stringT],
  }),
  ping: pluckT({
    type: ["type", literalT("ping")],
  }),
  listen: pluckT({
    type: ["type", literalT("listen")],
    channel: ["channel", stringT],
  }),
  unlisten: pluckT({
    type: ["type", literalT("unlisten")],
    channel: ["channel", stringT],
  }),
});

export const clientFrameT = sumT("type", {
  connect: pluckT({
    type: ["type", literalT("connect")],
    auth: ["auth", stringT],
    streamId: ["stream-id", nullableT(stringT)],
    lastEventId: ["last-event-id", nullableT(numberT)],
  }),
  ack: pluckT({
    type: ["type", literalT("ack")],
    lastEventId: ["last-event-id", nullableT(numberT)],
  }),
  listen: pluckT({
    type: ["type", literalT("listen")],
    channel: ["channel", stringT],
  }),
  unlisten: pluckT({
    type: ["type", literalT("unlisten")],
    channel: ["channel", stringT],
  }),
});
