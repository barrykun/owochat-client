import { TypeOf, dumpJson, arrayT, booleanT, pluckT, stringT, nullableT } from "../util/decoder";
import {
  conversationT,
  directMessageT,
  emojiT,
  messageT,
  putRoomInfoT,
  putUserInfoT,
  roomInfoT,
  roomSearchRecordT,
  roomUserRoleT,
  serverConfigT,
  sessionInfoT,
  userDeviceMetaT,
  userInfoT,
  userPermissionsT,
  userSearchRecordT,
  webpushDataT,
} from "./data";
import { Endpoint, EndpointResult, jsonDecoder, throwingJsonDecoder } from "../util/http";

type Endpoints = {
  // Config
  getConfig: Endpoint<{}, TypeOf<typeof serverConfigT>>;

  // Session
  getSession: Endpoint<{}, EndpointResult<TypeOf<typeof sessionInfoT>>>;

  postSession: Endpoint<
    { username: string; password: string; deviceToken: string; deviceName: string },
    { username: string; token: string }
  >;

  // Invites
  getInvite: Endpoint<{ inviteToken: string }, { inviter: string }>;

  postInvite: Endpoint<{ autoAdd: boolean }, { inviteToken: string }>;

  // Users
  postUser: Endpoint<
    { username: string; password: string; inviteToken: string | null },
    { username: string }
  >;

  getUserInfo: Endpoint<{ username: string }, TypeOf<typeof userInfoT>>;

  putUserInfo: Endpoint<
    { username: string; userInfo: TypeOf<typeof putUserInfoT> },
    TypeOf<typeof userInfoT>
  >;

  putUserPermissions: Endpoint<
    { username: string; isAdmin: boolean; canInvite: boolean },
    TypeOf<typeof userPermissionsT>
  >;

  getUserEmoji: Endpoint<{ username: string }, TypeOf<typeof emojiT>[]>;

  putUserEmoji: Endpoint<
    { username: string; shortcode: string; slug: string },
    TypeOf<typeof emojiT>
  >;

  deleteUserEmoji: Endpoint<{ username: string; shortcode: string }, {}>;

  getUserStarredRooms: Endpoint<
    { username: string },
    { rooms: { roomname: string; notifyMode: string }[] }
  >;

  putUserStarredRoom: Endpoint<{ username: string; roomname: string; notifyMode: string }, {}>;

  deleteUserStarredRoom: Endpoint<{ username: string; roomname: string }, {}>;

  getUserFriends: Endpoint<{ username: string }, { friends: { username: string }[] }>;

  putUserFriend: Endpoint<{ username: string; friend: string }, {}>;

  deleteUserFriend: Endpoint<{ username: string; friend: string }, {}>;

  getUserDevices: Endpoint<{ username: string }, TypeOf<typeof userDeviceMetaT>[]>;

  getUserDevice: Endpoint<
    { username: string; deviceToken: string },
    TypeOf<typeof userDeviceMetaT>
  >;

  deleteUserDevice: Endpoint<{ username: string; deviceToken: string }, {}>;

  putUserDevicePush: Endpoint<
    { username: string; deviceToken: string; data: TypeOf<typeof webpushDataT> },
    {}
  >;

  deleteUserDevicePush: Endpoint<{ username: string; deviceToken: string }, {}>;

  getSearchUsers: Endpoint<{ name: string }, { users: TypeOf<typeof userSearchRecordT>[] }>;

  // Direct Messages
  getDirectMessages: Endpoint<
    { username: string; start: string | null; end: string | null; limit: number | null },
    TypeOf<typeof directMessageT>[]
  >;

  postDirectMessage: Endpoint<{ username: string; enc: string; body: string }, { msgid: string }>;

  putDirectMessage: Endpoint<{ username: string; msgid: string; enc: string; body: string }, {}>;

  deleteDirectMessage: Endpoint<{ username: string; msgid: string }, {}>;

  // Conversations
  getOpenConversations: Endpoint<{}, { conversations: TypeOf<typeof conversationT>[] }>;

  getConversation: Endpoint<{ partner: string }, TypeOf<typeof conversationT>>;

  postConversationReadUntil: Endpoint<{ partner: string; lastReadMsgid: string }, {}>;

  putConversationOpened: Endpoint<{ partner: string; opened: boolean }, {}>;

  // Room Messages
  getRoomMessages: Endpoint<
    { roomname: string; start: string | null; end: string | null; limit: number | null },
    TypeOf<typeof messageT>[]
  >;

  postRoomMessage: Endpoint<{ roomname: string; body: string }, { msgid: string }>;

  putRoomMessage: Endpoint<{ roomname: string; msgid: string; body: string }, {}>;

  deleteRoomMessage: Endpoint<{ roomname: string; msgid: string }, {}>;

  // Room
  postRoom: Endpoint<{ roomname: string; membersOnly: boolean }, {}>;

  getRoomInfo: Endpoint<{ roomname: string }, TypeOf<typeof roomInfoT>>;

  putRoomInfo: Endpoint<
    { roomname: string; roomInfo: TypeOf<typeof putRoomInfoT> },
    TypeOf<typeof roomInfoT>
  >;

  getRoomEmoji: Endpoint<{ roomname: string }, TypeOf<typeof emojiT>[]>;

  putRoomEmoji: Endpoint<
    { roomname: string; shortcode: string; slug: string },
    TypeOf<typeof emojiT>
  >;

  deleteRoomEmoji: Endpoint<{ roomname: string; shortcode: string }, {}>;

  getRoomRoles: Endpoint<{ roomname: string }, { roles: TypeOf<typeof roomUserRoleT>[] }>;

  getRoomRole: Endpoint<{ roomname: string; username: string }, TypeOf<typeof roomUserRoleT>>;

  putRoomRole: Endpoint<{ roomname: string; username: string; role: string }, {}>;

  getSearchRooms: Endpoint<{ name: string }, { rooms: TypeOf<typeof roomSearchRecordT>[] }>;

  // Tick
  postTick: Endpoint<{ dst: string; code: string }, {}>;
};

export const endpoints: Endpoints = {
  // Config
  getConfig: new Endpoint({
    url: (_req) => "/api/config",
    decoder: throwingJsonDecoder(serverConfigT),
  }),

  // Session
  getSession: new Endpoint({
    url: (_req) => "/api/sessions",
    decoder: jsonDecoder(sessionInfoT),
  }),
  postSession: new Endpoint({
    method: "POST",
    url: (_req) => "/api/sessions",
    encoder: (req) =>
      dumpJson(
        pluckT({
          username: ["username", stringT],
          password: ["password", stringT],
          deviceToken: ["device-token", stringT],
          deviceName: ["device-name", stringT],
        }),
        req
      ),
    decoder: throwingJsonDecoder(
      pluckT({ username: ["username", stringT], token: ["token", stringT] })
    ),
  }),

  // Invites
  getInvite: new Endpoint({
    url: (req) => "/api/invites/" + req.inviteToken,
    decoder: throwingJsonDecoder(pluckT({ inviter: ["inviter", stringT] })),
  }),
  postInvite: new Endpoint({
    method: "POST",
    url: (_req) => "/api/invites",
    encoder: (req) =>
      dumpJson(pluckT({ autoAdd: ["auto-add", booleanT] }), { autoAdd: req.autoAdd }),
    decoder: throwingJsonDecoder(pluckT({ inviteToken: ["invite-token", stringT] })),
  }),

  // User
  postUser: new Endpoint({
    method: "POST",
    url: (_req) => "/api/users",
    encoder: (req) =>
      dumpJson(
        pluckT({
          username: ["username", stringT],
          password: ["password", stringT],
          inviteToken: ["invite-token", nullableT(stringT)],
        }),
        req
      ),
    decoder: throwingJsonDecoder(pluckT({ username: ["username", stringT] })),
  }),
  getUserInfo: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/info",
    decoder: throwingJsonDecoder(userInfoT),
  }),
  putUserPermissions: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/permissions",
    encoder: (req) =>
      dumpJson(userPermissionsT, { isAdmin: req.isAdmin, canInvite: req.canInvite }),
  }),
  putUserInfo: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/info",
    encoder: (req) => dumpJson(putUserInfoT, req.userInfo),
    decoder: throwingJsonDecoder(userInfoT),
  }),
  getUserEmoji: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/emoji",
    decoder: throwingJsonDecoder(arrayT(emojiT)),
  }),
  putUserEmoji: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/emoji/" + req.shortcode,
    encoder: (req) => dumpJson(emojiT, { shortcode: req.shortcode, slug: req.slug }),
    decoder: throwingJsonDecoder(emojiT),
  }),
  deleteUserEmoji: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/users/" + req.username + "/emoji/" + req.shortcode,
  }),
  getUserStarredRooms: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/starred",
    decoder: throwingJsonDecoder(
      pluckT({
        rooms: [
          "rooms",
          arrayT(pluckT({ roomname: ["roomname", stringT], notifyMode: ["notify-mode", stringT] })),
        ],
      })
    ),
  }),
  putUserStarredRoom: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/starred/" + req.roomname,
    encoder: (req) =>
      dumpJson(pluckT({ notifyMode: ["notify-mode", stringT] }), { notifyMode: req.notifyMode }),
  }),
  deleteUserStarredRoom: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/users/" + req.username + "/starred/" + req.roomname,
  }),
  getUserFriends: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/friends",
    decoder: throwingJsonDecoder(
      pluckT({
        friends: [
          "friends",
          arrayT(
            pluckT({
              username: ["username", stringT],
            })
          ),
        ],
      })
    ),
  }),
  putUserFriend: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/friends/" + req.friend,
  }),
  deleteUserFriend: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/users/" + req.username + "/friends/" + req.friend,
  }),
  getUserDevices: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/devices",
    decoder: throwingJsonDecoder(arrayT(userDeviceMetaT)),
  }),
  getUserDevice: new Endpoint({
    url: (req) => "/api/users/" + req.username + "/devices/" + req.deviceToken,
    decoder: throwingJsonDecoder(userDeviceMetaT),
  }),
  deleteUserDevice: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/users/" + req.username + "/devices/" + req.deviceToken,
  }),
  putUserDevicePush: new Endpoint({
    method: "PUT",
    url: (req) => "/api/users/" + req.username + "/devices/" + req.deviceToken + "/push",
    encoder: (req) => dumpJson(webpushDataT, req.data),
  }),
  deleteUserDevicePush: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/users/" + req.username + "/devices/" + req.deviceToken + "/push",
  }),
  getSearchUsers: new Endpoint({
    url: (_req) => "/api/search/users",
    query: (req) => ({ name: req.name }),
    decoder: throwingJsonDecoder(
      pluckT({
        users: ["users", arrayT(userSearchRecordT)],
      })
    ),
  }),

  // Direct Messages
  getDirectMessages: new Endpoint({
    method: "GET",
    url: (req) => "/api/dm/" + req.username + "/messages",
    query: (req) => ({ start: req.start, end: req.end, limit: req.limit }),
    decoder: throwingJsonDecoder(arrayT(directMessageT)),
  }),
  postDirectMessage: new Endpoint({
    method: "POST",
    url: (req) => "/api/dm/" + req.username + "/messages",
    encoder: (req) =>
      dumpJson(
        pluckT({ username: ["username", stringT], enc: ["enc", stringT], body: ["body", stringT] }),
        req
      ),
    decoder: throwingJsonDecoder(pluckT({ msgid: ["msgid", stringT] })),
  }),
  putDirectMessage: new Endpoint({
    method: "PUT",
    url: (req) => "/api/dm/" + req.username + "/messages/" + req.msgid,
    encoder: (req) =>
      dumpJson(
        pluckT({
          username: ["username", stringT],
          msgid: ["msgid", stringT],
          enc: ["enc", stringT],
          body: ["body", stringT],
        }),
        req
      ),
  }),
  deleteDirectMessage: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/dm/" + req.username + "/messages/" + req.msgid,
  }),

  // Conversations
  getOpenConversations: new Endpoint({
    method: "GET",
    url: (_req) => "/api/conversations",
    decoder: throwingJsonDecoder(
      pluckT({ conversations: ["conversations", arrayT(conversationT)] })
    ),
  }),

  getConversation: new Endpoint({
    method: "GET",
    url: (req) => "/api/conversations/" + req.partner,
    decoder: throwingJsonDecoder(conversationT),
  }),

  postConversationReadUntil: new Endpoint({
    method: "POST",
    url: (req) => "/api/conversations/" + req.partner + "/read-until",
    encoder: (req) => dumpJson(pluckT({ lastReadMsgid: ["last-read-msgid", stringT] }), req),
  }),

  putConversationOpened: new Endpoint({
    method: "PUT",
    url: (req) => "/api/conversations/" + req.partner + "/opened",
    encoder: (req) => dumpJson(pluckT({ opened: ["opened", booleanT] }), req),
  }),

  // Room Messages
  getRoomMessages: new Endpoint({
    method: "GET",
    url: (req) => "/api/rooms/" + req.roomname + "/messages",
    query: (req) => ({ start: req.start, end: req.end, limit: req.limit }),
    decoder: throwingJsonDecoder(arrayT(messageT)),
  }),
  postRoomMessage: new Endpoint({
    method: "POST",
    url: (req) => "/api/rooms/" + req.roomname + "/messages",
    encoder: (req) =>
      dumpJson(pluckT({ roomname: ["roomname", stringT], body: ["body", stringT] }), req),
    decoder: throwingJsonDecoder(messageT),
  }),
  putRoomMessage: new Endpoint({
    method: "PUT",
    url: (req) => "/api/rooms/" + req.roomname + "/messages/" + req.msgid,
    encoder: (req) =>
      dumpJson(
        pluckT({
          roomname: ["roomname", stringT],
          msgid: ["msgid", stringT],
          body: ["body", stringT],
        }),
        req
      ),
  }),
  deleteRoomMessage: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/rooms/" + req.roomname + "/messages/" + req.msgid,
  }),

  // Room
  postRoom: new Endpoint({
    method: "POST",
    url: (_req) => "/api/rooms",
    encoder: (req) =>
      dumpJson(
        pluckT({ roomname: ["roomname", stringT], membersOnly: ["members-only", booleanT] }),
        { roomname: req.roomname, membersOnly: req.membersOnly }
      ),
  }),
  getRoomInfo: new Endpoint({
    url: (req) => "/api/rooms/" + req.roomname + "/info",
    decoder: throwingJsonDecoder(roomInfoT),
  }),
  putRoomInfo: new Endpoint({
    method: "PUT",
    url: (req) => "/api/rooms/" + req.roomname + "/info",
    encoder: (req) => dumpJson(putRoomInfoT, req.roomInfo),
    decoder: throwingJsonDecoder(roomInfoT),
  }),
  getRoomEmoji: new Endpoint({
    url: (req) => "/api/rooms/" + req.roomname + "/emoji",
    decoder: throwingJsonDecoder(arrayT(emojiT)),
  }),
  putRoomEmoji: new Endpoint({
    method: "PUT",
    url: (req) => "/api/rooms/" + req.roomname + "/emoji/" + req.shortcode,
    encoder: (req) => dumpJson(emojiT, { shortcode: req.shortcode, slug: req.slug }),
    decoder: throwingJsonDecoder(emojiT),
  }),
  deleteRoomEmoji: new Endpoint({
    method: "DELETE",
    url: (req) => "/api/rooms/" + req.roomname + "/emoji/" + req.shortcode,
  }),
  getRoomRoles: new Endpoint({
    url: (req) => "/api/rooms/" + req.roomname + "/roles",
    decoder: throwingJsonDecoder(
      pluckT({
        roles: ["roles", arrayT(roomUserRoleT)],
      })
    ),
  }),
  getRoomRole: new Endpoint({
    url: (req) => "/api/rooms/" + req.roomname + "/roles/" + req.username,
    decoder: throwingJsonDecoder(roomUserRoleT),
  }),
  putRoomRole: new Endpoint({
    method: "PUT",
    url: (req) => "/api/rooms/" + req.roomname + "/roles/" + req.username,
    encoder: (req) => dumpJson(roomUserRoleT, { username: req.username, role: req.role }),
  }),
  getSearchRooms: new Endpoint({
    url: (_req) => "/api/search/rooms",
    query: (req) => ({ name: req.name }),
    decoder: throwingJsonDecoder(
      pluckT({
        rooms: ["rooms", arrayT(roomSearchRecordT)],
      })
    ),
  }),

  // Tick
  postTick: new Endpoint({
    method: "POST",
    url: (_req) => "/api/ticks",
    encoder: (req) =>
      dumpJson(pluckT({ dst: ["dst", stringT], code: ["code", stringT] }), {
        dst: req.dst,
        code: req.code,
      }),
  }),
};
