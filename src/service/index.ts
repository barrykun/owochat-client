import { Writable, Readable, writable } from "svelte/store";
import { TypeOf } from "../util/decoder";
import { endpoints } from "./endpoints";
import { serverConfigT } from "./data";
import Session from "./session";

type Config = TypeOf<typeof serverConfigT>;

interface BeforeInstallPromptEvent {
  prompt(): void;
  preventDefault(): Promise<undefined>;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type Notification = {
  type: "room";
  roomname: string;
};

type Permissions = {
  canInvite: boolean;
};

function generateDeviceToken(): string {
  const bytes = new Uint8Array(16);
  crypto.getRandomValues(bytes);
  let out = "";
  for (const byte of bytes) {
    const hex = byte.toString(16);
    if (byte < 0x10) {
      out += "0";
      out += hex;
    } else {
      out += hex;
    }
  }
  return out;
}

function sessionsEqual(a: Session, b: Session): boolean {
  return (
    a.authorization === b.authorization &&
    a.myUsername === b.myUsername &&
    a.deviceToken === b.deviceToken
  );
}

export class Service {
  private _session: Session | null;
  private _sessionStore: Writable<Session | null>;
  private _offline: boolean;
  private _offlineStore: Writable<boolean>;
  private _beforeInstallPromptStore: Writable<BeforeInstallPromptEvent | null>;
  private _configStore: Writable<Config | null>;
  private _myPermissionsStore: Writable<Permissions>;

  public constructor() {
    const auth = localStorage.getItem("auth-token");
    const myUsername = localStorage.getItem("auth-username");
    if (auth && myUsername) {
      const deviceToken = fetchDeviceToken(myUsername);
      this._session = new Session(auth, myUsername, deviceToken);
    } else {
      this._session = null;
    }
    this._offline = false;
    this._offlineStore = writable(false);
    this._sessionStore = writable(this._session);
    this._beforeInstallPromptStore = writable(null);
    this._configStore = writable(null);
    this._myPermissionsStore = writable({ canInvite: false });
    if (auth) {
      this._checkAuth(auth);
    }
    this._fetchConfig();
  }

  public getSessionStore(): Readable<Session | null> {
    return this._sessionStore;
  }

  private async _fetchConfig() {
    const config = await endpoints.getConfig.call({ authorization: null }, {});
    this._configStore.set(config);
  }

  private async _checkAuth(authorization: string) {
    try {
      const res = await endpoints.getSession.call({ authorization }, {});
      if (res.isOk) {
        const { username, permissions } = res.value;
        localStorage.setItem("auth-token", authorization);
        localStorage.setItem("auth-username", username);
        const deviceToken = fetchDeviceToken(username);
        const newSession = new Session(authorization, username, deviceToken);
        if (!this._session || !sessionsEqual(newSession, this._session)) {
          this._session = newSession;
          this._sessionStore.set(this._session);
        }
        this._myPermissionsStore.set({
          canInvite: permissions.canInvite,
        });
      } else {
        this._session = null;
        this._sessionStore.set(null);
      }
      this._offline = false;
      this._offlineStore.set(false);
    } catch (_e) {
      this._offline = true;
      this._offlineStore.set(true);
      // TODO: handle network error, go into offline mode
    }
  }

  public async login(username: string, password: string) {
    username = username.toLowerCase();
    const deviceToken = fetchDeviceToken(username);
    const { token } = await endpoints.postSession.call(
      { authorization: null },
      {
        username,
        password,
        deviceToken,
        deviceName: "Browser",
      }
    );
    localStorage.setItem("auth-token", token);
    localStorage.setItem("auth-username", username);
    this._session = new Session(token, username, deviceToken);
    this._sessionStore.set(this._session);
  }

  public async logout() {
    // TODO: delete session
    localStorage.removeItem("auth-token");
    localStorage.removeItem("auth-username");
    // TODO: make it not require a refresh
    window.location.reload();
  }

  public async register(username: string, password: string) {
    username = username.toLowerCase();
    await endpoints.postUser.call(
      { authorization: null },
      { username, password, inviteToken: null }
    );
    await this.login(username, password);
  }

  public async registerWithToken(username: string, password: string, inviteToken: string) {
    username = username.toLowerCase();
    await endpoints.postUser.call({ authorization: null }, { username, password, inviteToken });
    await this.login(username, password);
  }

  public async getInviteInfo(inviteToken: string): Promise<{ inviter: string }> {
    const { inviter } = await endpoints.getInvite.call({ authorization: null }, { inviteToken });
    return {
      inviter,
    };
  }

  public async createInvite(config: { autoAdd: boolean }): Promise<{ inviteToken: string }> {
    if (!this._session) {
      throw new Error("invalid state");
    }
    const { inviteToken } = await this._session.call(endpoints.postInvite, {
      autoAdd: config.autoAdd,
    });
    return {
      inviteToken,
    };
  }

  public onBeforeInstallPrompt(event: any) {
    this._beforeInstallPromptStore.set(event);
  }

  public getBeforeInstallPromptStore(): Readable<BeforeInstallPromptEvent | null> {
    return this._beforeInstallPromptStore;
  }

  public getConfigStore(): Readable<Config | null> {
    return this._configStore;
  }

  public getMyPermissionsStore(): Readable<Permissions> {
    return this._myPermissionsStore;
  }

  public getOfflineStore(): Readable<boolean> {
    return this._offlineStore;
  }
}

function fetchDeviceToken(username: string): string {
  let deviceToken = localStorage.getItem(username + ":device-token");
  if (!deviceToken) {
    deviceToken = generateDeviceToken();
    localStorage.setItem(username + ":device-token", deviceToken);
  }
  return deviceToken;
}

const service = new Service();

export default service;
